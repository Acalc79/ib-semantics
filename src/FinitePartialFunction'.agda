{-# OPTIONS --exact-split --safe  #-}
module FinitePartialFunction' where

open import Level
open import Data.Product hiding (map)
open import Data.Maybe hiding (map)
open import Data.List hiding (lookup; [_])
open import Data.List.Membership.Propositional using (_∈_) public

private
  variable a b : Level
           A : Set a
           B : A → Set a

  module Private where
    record _⇀_ (A : Set a)(B : A → Set b) : Set (a ⊔ b) where
      constructor [_,_,_]
      field
        dom : List A
        partial : (a : A) → Maybe (B a)
        prop : ∀{a}.(p : a ∈ dom) → Is-just (partial a)

open Private
open Private renaming (_⇀_ to _⇀fin_) public
open _⇀_ using (dom) public

⦃⦄ : A ⇀ B
⦃⦄ = [ [] , (λ _ → nothing) , (λ ()) ]

_+[_↦_] : (f : A ⇀ B)(a : A)(b : B a) → A ⇀ B
[ d , ls , p ] +[ a ↦ b ] =
  [ a ∷ d , (λ a' → {!!}) , {!!} ]

open import Data.List.Relation.Unary.Any hiding (lookup)

lookup : (f : A ⇀ B)(a : A).(p : a ∈ dom f) → B a
lookup = {!!}
-- lookup [ (_ , n) ∷ _ ] _ (here _) = n
-- lookup [ _ ∷ ls ] a (there p) = lookup [ ls ] a p

syntax lookup s ℓ p = s lookup[ p ] ℓ
