{-# OPTIONS --exact-split --safe  #-}
module L2.Properties where

open import L2.Definition
open import L2.Properties.Base

open import Relation.Nullary
open import Relation.Nullary.Decidable
open import Relation.Binary.PropositionalEquality

open import Data.Product renaming (_×_ to _∧_)
open import Data.Sum renaming (_⊎_ to _∨_)
open import Data.Integer as ℤ

-- Theorem 1
determinacy : ∀{e s e₁ s₁ e₂ s₂} →
  〈 e ، s 〉 ⇝ 〈 e₁ ، s₁ 〉 →
  〈 e ، s 〉 ⇝ 〈 e₂ ، s₂ 〉 →
  ------------------------------
  〈 e₁ ، s₁ 〉 ≡ 〈 e₂ ، s₂ 〉

open import Data.List.Relation.Binary.Subset.Propositional

open import FinitePartialFunction

-- Theorem 2
progress : ∀{Γ e T s} →
  Γ ⊢ e ∶ T →
  dom (proj₁ Γ) ⊆ dom s →
  ------------------------------
  value e ∨ ∃₂ λ e' s' → 〈 e ، s 〉 ⇝ 〈 e' ، s' 〉

-- Theorem 3
type-preservation : ∀{Γ e T s e' s'} →
  Γ ⊢ e ∶ T →
  dom (proj₁ Γ) ⊆ dom s →
  〈 e ، s 〉 ⇝ 〈 e' ، s' 〉 →
  ------------------------------
  Γ ⊢ e' ∶ T ∧ dom (proj₁ Γ) ⊆ dom s'

open import Relation.Binary.Construct.Closure.ReflexiveTransitive
_⇝*_ = Star _⇝_

-- Theorem 4
safety : ∀{Γ e T s e' s'} →
  Γ ⊢ e ∶ T →
  dom (proj₁ Γ) ⊆ dom s →
  〈 e ، s 〉 ⇝* 〈 e' ، s' 〉 →
  ------------------------------
  value e' ∨ ∃₂ λ e″ s″ → 〈 e' ، s' 〉 ⇝ 〈 e″ ، s″ 〉

open import Data.Bool hiding (_∧_; _∨_)

determinacy = {!!}
-- {while e do' e₁} while while = refl
-- determinacy {(! ℓ₁)} (deref _ refl)(deref _ refl) = refl
-- determinacy {ℓ₁ := e} (assign2 p')(assign2 p'')
--   with refl ← determinacy p' p'' = refl
-- determinacy {ℓ₁ := n-val n}(assign1 _)(assign1 _) = refl
-- determinacy {.skip ⍮ e₁} seq1 seq1 = refl
-- determinacy {e₁ ⍮ e₂}(seq2 p')(seq2 p'') with refl ← determinacy p' p'' = refl
-- determinacy {_ ′ _ ′ _}(op+ refl)(op+ refl) = refl
-- determinacy {_ ′ _ ′ _}(op≥ refl)(op≥ refl) = refl
-- determinacy {e₁ ′ op ′ e₂}(op1 p')(op1 p'') with refl ← determinacy p' p'' = refl
-- determinacy {e₁ ′ op ′ e₂}(op2 _ p')(op2 _ p'')
--   with refl ← determinacy p' p'' = refl
-- determinacy {e₁ ′ op ′ e₂}(op1 {s = s}{e₁'}{s'} p')(op2 value-e₁ p'')
--   with () ← values-don't-reduce value-e₁ s e₁' s' p'
-- determinacy {e₁ ′ op ′ e₂}(op2 value-e₁ p')(op1 {s = s}{e₁''}{s''} p'')
--   with () ← values-don't-reduce value-e₁ s e₁'' s'' p''
-- determinacy {if .(b-val true) then e₁ else' e₂} if1 if1 = refl
-- determinacy {if .(b-val false) then e₁ else' e₂} if2 if2 = refl
-- determinacy {if e then e₁ else' e₂}(if3 p')(if3 p'')
--   with refl ← determinacy p' p'' = refl

open import Function
open DecidableA 𝕃-mod._≟_

progress = {!!}
-- int Γ⊆s = inj₁ n-val
-- progress bool Γ⊆s = inj₁ b-val
-- progress skip Γ⊆s = inj₁ skip
-- progress {s = s} (op+ {e₁ = e₁}{e₂ = e₂} e₁:int e₂:int) Γ⊆s
--   with progress e₁:int Γ⊆s
-- ... | inj₂ (e₁' , s' , e₁s⇝e₁'s') =
--   inj₂ (e₁' ′ + ′ e₂ , s' , op1 e₁s⇝e₁'s')
-- ... | inj₁ value-e₁ with progress e₂:int Γ⊆s
-- ... | inj₂ (e₂' , s' , e₂s⇝e₂'s') =
--   inj₂ (e₁ ′ + ′ e₂' , s' , op2 value-e₁ e₂s⇝e₂'s')
-- progress {s = s} _ _ | inj₁ n-val | inj₁ n-val = inj₂ (n-val , s , op+ refl)
-- progress {s = s} (op≥ {e₁ = e₁}{e₂ = e₂} e₁:int e₂:int) Γ⊆s
--   with progress e₁:int Γ⊆s
-- ... | inj₂ (e₁' , s' , e₁s⇝e₁'s') =
--   inj₂ (e₁' ′ ≥ ′ e₂ , s' , op1 e₁s⇝e₁'s')
-- ... | inj₁ value-e₁ with progress e₂:int Γ⊆s
-- ... | inj₂ (e₂' , s' , e₂s⇝e₂'s') =
--   inj₂ (e₁ ′ ≥ ′ e₂' , s' , op2 value-e₁ e₂s⇝e₂'s')
-- progress {s = s} _ _ | inj₁ n-val | inj₁ n-val = inj₂ (b-val , s , op≥ refl)
-- progress {s = s} (if {e₂ = e₂}{e₃ = e₃} e₁:bool e₂:T e₃:T) Γ⊆s
--   with progress e₁:bool Γ⊆s
-- ... | inj₁ (b-val {b = true}) = inj₂ (e₂ , s , if1 e₂ e₃ s)
-- ... | inj₁ (b-val {b = false}) = inj₂ (e₃ , s , if2 e₂ e₃ s)
-- ... | inj₂ (e₁' , s' , e₁s⇝e₁'s') =
--   inj₂ (if e₁' then e₂ else' e₃ , s' , if3 e₁s⇝e₁'s')
-- progress {s = s} (assign {ℓ = ℓ} ℓ∈Γ refl e:int) Γ⊆s with progress e:int Γ⊆s
-- ... | inj₁ (n-val {n = n}) = inj₂ (skip , s +[ ℓ ↦ n ] , assign1 (Γ⊆s ℓ∈Γ))
-- ... | inj₂ (e' , s' , es⇝e's') =
--   inj₂ (ℓ := e' , s' , assign2 es⇝e's')
-- progress {s = s} (deref ℓ₀ p Γ[l]=intref) Γ⊆s =
--   inj₂ (n-val (s lookup[ Γ⊆s p ] ℓ₀) , s , deref ℓ₀ s _ (Γ⊆s p) refl)
-- progress {s = s} (seq e₁ e₂ _ e₁:unit e₂:T) Γ⊆s with progress e₁:unit Γ⊆s
-- ... | inj₁ skip = inj₂ (e₂ , s , seq1 e₂ s)
-- ... | inj₂ (e₁' , s' , e₁s⇝e₁'s') =
--   inj₂ (e₁' ⍮ e₂ , s' , seq2 e₁ e₂ s e₁' s' e₁s⇝e₁'s')
-- progress {s = s} (while e₁ e₂ e₁:bool e₂:unit) Γ⊆s =
--   inj₂ (if e₁ then e₂ ⍮ (while e₁ do' e₂) else' skip , s , while e₁ e₂ s)

typ-preserv : ∀{Γ e T s e' s'}
  (e⇝e' : 〈 e ، s 〉 ⇝ 〈 e' ، s' 〉)
  (e:T : Γ ⊢ e ∶ T)
  (Γ⊆s : dom (proj₁ Γ) ⊆ dom s)
  → ------------------------------
  Γ ⊢ e' ∶ T ∧ dom (proj₁ Γ) ⊆ dom s'

typ-preserv (op+ _) (op+ _ _) Γ⊆s = int , Γ⊆s
typ-preserv (op≥ _) (op≥ _ _) Γ⊆s = bool , Γ⊆s
typ-preserv (op1 e₁⇝e₁') (op+ e₁:int e₂:int) Γ⊆s
  with e₁':int , Γ⊆s' ← typ-preserv e₁⇝e₁' e₁:int Γ⊆s =
  op+ e₁':int e₂:int , Γ⊆s'
typ-preserv (op1 e₁⇝e₁') (op≥ e₁:int e₂:int) Γ⊆s
  with e₁':int , Γ⊆s' ← typ-preserv e₁⇝e₁' e₁:int Γ⊆s =
  op≥ e₁':int e₂:int , Γ⊆s'
typ-preserv (op2 _ e₂⇝e₂') (op+ e₁:int e₂:int) Γ⊆s
  with e₂':int , Γ⊆s' ← typ-preserv e₂⇝e₂' e₂:int Γ⊆s =
  op+ e₁:int e₂':int , Γ⊆s'
typ-preserv (op2 _ e₂⇝e₂') (op≥ e₁:int e₂:int) Γ⊆s
  with e₂':int , Γ⊆s' ← typ-preserv e₂⇝e₂' e₂:int Γ⊆s =
  op≥ e₁:int e₂':int , Γ⊆s'
typ-preserv (deref p x) (deref ℓ∈Γ Γ[l]=intref) Γ⊆s = int , Γ⊆s
typ-preserv {Γ = Γ} (assign1 {ℓ}{s}{n} x) (assign ℓ∈Γ Γ[l]=intref e:T) Γ⊆s =
  skip , λ {ℓ₀} → Equivalence.g (∈dom+⇔dom s ℓ n (Γ⊆s ℓ∈Γ) ℓ₀) ∘ Γ⊆s
typ-preserv (assign2 e⇝e') (assign ℓ∈Γ refl e:int) Γ⊆s
  with e':int , Γ⊆s' ← typ-preserv e⇝e' e:int Γ⊆s =
  assign ℓ∈Γ refl e':int , Γ⊆s'
typ-preserv seq1 (seq _ e₂:T) Γ⊆s = e₂:T , Γ⊆s
typ-preserv (seq2 e₁⇝e₁') (seq e₁:unit e₂:T) Γ⊆s
  with e₁':unit , Γ⊆s' ← typ-preserv e₁⇝e₁' e₁:unit Γ⊆s =
  seq e₁':unit e₂:T , Γ⊆s'
typ-preserv if1 (if _ e₂:T _) Γ⊆s = e₂:T , Γ⊆s
typ-preserv if2 (if _ _ e₃:T) Γ⊆s = e₃:T , Γ⊆s
typ-preserv (if3 e₁⇝e₁')(if e₁:bool e₂:T e₃:T) Γ⊆s
  with e₁':bool , Γ⊆s' ← typ-preserv e₁⇝e₁' e₁:bool Γ⊆s =
  if e₁':bool e₂:T e₃:T , Γ⊆s'
typ-preserv while e:unit@(while e₁:bool e₂:unit) Γ⊆s =
  if e₁:bool (seq e₂:unit e:unit) skip , Γ⊆s
typ-preserv (app1 e₁⇝e₁') (app e₁:T₁→T₂ e₂:T₂) Γ⊆s
  with e₁':T₁→T₂ , Γ⊆s' ← typ-preserv e₁⇝e₁' e₁:T₁→T₂ Γ⊆s =
  app e₁':T₁→T₂ e₂:T₂ , Γ⊆s'
typ-preserv (app2 _ e₂⇝e₂') (app e₁:T₁→T₂ e₂:T₂) Γ⊆s
  with e₂':T₂ , Γ⊆s' ← typ-preserv e₂⇝e₂' e₂:T₂ Γ⊆s =
  app e₁:T₁→T₂ e₂':T₂ , Γ⊆s'
typ-preserv (fn value-v) e:T Γ⊆s = {!!} , {!!}
typ-preserv (let1 e⇝e') e:T Γ⊆s = {!!}
typ-preserv (let2 x) e:T Γ⊆s = {!!}
typ-preserv letrecfn e:T Γ⊆s = {!!}
-- type-preservation (if .(b-val true) e₂ _ T _ e₂:T _) Γ⊆s (if1 .e₂ _ _) =
--   e₂:T , Γ⊆s
-- type-preservation (if .(b-val false) _ e₃ T _ _ e₃:T) Γ⊆s (if2 _ .e₃ _) =
--   e₃:T , Γ⊆s
-- type-preservation (if e₁ e₂ e₃ T e₁:bool e₂:T e₃:T) Γ⊆s
--                   (if3 .e₁ .e₂ .e₃ _ e₁' _ e₁s⇝e₁'s')
--   with e₁':bool , Γ⊆s' ← type-preservation e₁:bool Γ⊆s e₁s⇝e₁'s' =
--   if e₁' e₂ e₃ T e₁':bool e₂:T e₃:T , Γ⊆s'
-- type-preservation (assign ℓ₀ .(n-val n) p _ e:T) Γ⊆s (assign1 .ℓ₀ s n x) =
--   skip , λ {x} x∈domΓ → Equivalence.g (∈dom+⇔dom s ℓ₀ n (Γ⊆s p) x) (Γ⊆s x∈domΓ)
-- type-preservation (assign ℓ₀ e p _ e:int) Γ⊆s (assign2 .e _ e' _ .ℓ₀ es⇝e's')
--   with e':int , Γ⊆s' ← type-preservation e:int Γ⊆s es⇝e's' =
--   assign ℓ₀ e' p refl e':int , Γ⊆s'
-- type-preservation (deref ℓ₀ p Γ[l]=intref) Γ⊆s (deref .ℓ₀ _ n p₁ x) = int n , Γ⊆s
-- type-preservation (seq .skip e₂ _ _ e₂:T) Γ⊆s (seq1 .e₂ _) = e₂:T , Γ⊆s
-- type-preservation (seq e₁ e₂ T e₁:unit e₂:T) Γ⊆s (seq2 _ _ _ e₁' _ e₁s⇝e₁'s')
--   with e₁':unit , Γ⊆s' ← type-preservation e₁:unit Γ⊆s e₁s⇝e₁'s' =
--   seq e₁' e₂ T e₁':unit e₂:T , Γ⊆s'
-- type-preservation t@(while e₁ e₂ e₁:bool e₂:unit) Γ⊆s (while .e₁ .e₂ _) =
--   if e₁ (e₂ ⍮ (while e₁ do' e₂)) skip unit
--     e₁:bool
--     (seq e₂ (while e₁ do' e₂) unit e₂:unit t)
--     skip ,
--   Γ⊆s
type-preservation e:T Γ⊆s e⇝e' = typ-preserv e⇝e' e:T Γ⊆s

safety e:T Γ⊆s ε = progress e:T Γ⊆s
safety e:T Γ⊆s (es⇝e″s″ ◅ e″s″⇝*e's')
  with e″:T , Γ⊆s″ ← type-preservation e:T Γ⊆s es⇝e″s″ =
  safety e″:T Γ⊆s″ e″s″⇝*e's'
