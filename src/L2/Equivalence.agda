{-# OPTIONS --exact-split --safe  #-}
module L2.Equivalence where

open import L2.Definition

open import Relation.Binary.PropositionalEquality

open import FinitePartialFunction

data E : Set where
  — : E
  _↓′_′_ : (C[—] : E)(op : Op)(e₂ : L₂) → E
  _′_′_↓ : (e₁ : L₂)(op : Op)(C[—] : E) → E
  if_↓then_else'_ : (C[—] : E)(e₂ e₃ : L₂) → E
  if_then_↓else'_ : (e₁ : L₂)(C[—] : E)(e₃ : L₂) → E
  if_then_else'_↓ : (e₁ e₂ : L₂)(C[—] : E) → E
  _:=_↓ : (ℓ : 𝕃)(C[—] : E) → E
  _↓⍮_ : (C[—] : E)(e₂ : L₂) → E
  _⍮_↓ : (e₁ : L₂)(C[—] : E) → E
  while_↓do'_ : (C[—] : E)(e₂ : L₂) → E
  while_do'_↓ : (e₁ : L₂)(C[—] : E) → E
  fn_∶_⇒_↓ : (x : 𝕏)(T : 𝕋)(C[—] : E) → E
  _`_↓ : (e₁ : L₂)(C[—] : E) → E
  _↓`_ : (C[—] : E)(e₂ : L₂) → E
  let-val_∶_==_↓in'_end : (x : 𝕏)(T : 𝕋)(C[—] : E)(e₂ : L₂) → E
  let-val_∶_==_in'_↓end : (x : 𝕏)(T : 𝕋)(e₁ : L₂)(C[—] : E) → E
  let-val-rec_∶_—>_==fn_⇒_↓in'_end :
    (x : 𝕏)(T₁ T₂ : 𝕏)(y : 𝕏)(C[—] : E)(e₂ : L₂) → E
  let-val-rec_∶_—>_==fn_⇒_in'_↓end :
    (x : 𝕏)(T₁ T₂ : 𝕏)(y : 𝕏)(e₁ : L₂)(C[—] : E) → E

infixl 120 _[_/—]
_[_/—] : (C[—] : E)(e : L₂) → L₂
— [ e /—] = e
(C[—] ↓′ op ′ e₂) [ e /—] = C[—] [ e /—] ′ op ′ e₂
(e₁ ′ op ′ C[—] ↓) [ e /—] = e₁ ′ op ′ C[—] [ e /—]
(if C[—] ↓then e₂ else' e₃) [ e /—] = if C[—] [ e /—] then e₂ else' e₃
(if e₁ then C[—] ↓else' e₃) [ e /—] = if e₁ then C[—] [ e /—] else' e₃
if e₁ then e₂ else' C[—] ↓ [ e /—] = if e₁ then e₂ else' C[—] [ e /—] 
(ℓ := C[—] ↓) [ e /—] = ℓ := C[—] [ e /—] 
(C[—] ↓⍮ e₂) [ e /—] = C[—] [ e /—] ⍮ e₂
(e₁ ⍮ C[—] ↓) [ e /—] = e₁ ⍮ C[—] [ e /—] 
(while C[—] ↓do' e₂) [ e /—] = while C[—] [ e /—] do' e₂
while e₁ do' C[—] ↓ [ e /—] = while e₁ do' C[—] [ e /—] 
fn x ∶ T ⇒ C[—] ↓ [ e /—] = fn x ∶ T ⇒ C[—] [ e /—] 
(e₁ ` C[—] ↓) [ e /—] = e₁ ` C[—] [ e /—] 
(C[—] ↓` e₂) [ e /—] = C[—] [ e /—] ` e₂
let-val x ∶ T == C[—] ↓in' e₂ end [ e /—] =
  let-val x ∶ T == C[—] [ e /—] in' e₂ end
let-val x ∶ T == e₁ in' C[—] ↓end [ e /—] =
  let-val x ∶ T == e₁ in' C[—] [ e /—] end
let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ C[—] ↓in' e₂ end [ e /—] =
  let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ C[—] [ e /—] in' e₂ end
let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ e₁ in' C[—] ↓end [ e /—] =
  let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ e₁ in' C[—] [ e /—] end

data _α-ren≡_ : (e₁ e₂ : L₂) → Set where
  ren : ∀{e₁ e₂ x x' T}
    (p₁ : e₁ ≡ sub ｛ var x / x' ｝ e₂)
    (p₂ : e₂ ≡ sub ｛ var x' / x ｝ e₁)
    → ----------------------------------------
    fn x ∶ T ⇒ e₁ α-ren≡ fn x' ∶ T ⇒ e₂

  ctx : ∀{C[—] e₁ e₂}
    (p : e₁ α-ren≡ e₂)
    → ------------------------------
    C[—] [ e₁ /—] α-ren≡ C[—] [ e₂ /—]

open import Relation.Binary.Construct.Closure.ReflexiveTransitive
_≡α_ = Star _α-ren≡_

open import Level
open import Relation.Binary
import Relation.Binary.Construct.Closure.ReflexiveTransitive.Properties as P

sym-α-ren≡ : Symmetric _α-ren≡_
sym-α-ren≡ (ren p₁ p₂) = ren p₂ p₁
sym-α-ren≡ (ctx p) = ctx (sym-α-ren≡ p)

Setoid≡α : Setoid 0ℓ 0ℓ
Setoid.Carrier Setoid≡α = L₂
Setoid._≈_ Setoid≡α = _≡α_
IsEquivalence.refl (Setoid.isEquivalence Setoid≡α) = ε
IsEquivalence.sym (Setoid.isEquivalence Setoid≡α) = reverse sym-α-ren≡
IsEquivalence.trans (Setoid.isEquivalence Setoid≡α) = P.trans _α-ren≡_
