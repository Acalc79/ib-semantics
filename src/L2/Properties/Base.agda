{-# OPTIONS --exact-split --safe  #-}
module L2.Properties.Base where

open import L2.Definition

open import Relation.Nullary
open import Relation.Nullary.Decidable
open import Relation.Binary.PropositionalEquality

private
  variable
    Γ : TypeEnv
    e e' e₁ e₂ : L₂
    T T' T₁ T₂ : 𝕋
    s : Store

-- Lemma 9
values-don't-reduce :
  value e
  → ----------------------------------
  ∀ s e' s' → ¬ 〈 e ، s 〉 ⇝ 〈 e' ، s' 〉
values-don't-reduce n-val _ _ _ ()

open import Data.Product renaming (_×_ to _∧_)

-- Theorem 5
type-inference : ∀ Γ e → Dec (∃ λ T → Γ ⊢ e ∶ T)

-- Theorem 6
decidability-of-type-checking : ∀ Γ e T → Dec (Γ ⊢ e ∶ T)

-- Theorem 7
uniqueness-of-typing :
  Γ ⊢ e ∶ T →
  Γ ⊢ e ∶ T' →
  ----------------------------------------
  T ≡ T'

open import FinitePartialFunction

open import Data.List.Membership.DecPropositional 𝕃-mod._≟_
open import Relation.Nullary.Product
open import Function

_⊢?_∶_ : ∀ Γ e T → Dec (Γ ⊢ e ∶ T)
⊢! : ∀{Γ e T T'} → Γ ⊢ e ∶ T → Γ ⊢ e ∶ T' → T ≡ T'
infer : ∀ Γ e → Dec (∃ λ T → Γ ⊢ e ∶ T)

infer Γ (n-val n) = yes (int , int)
infer Γ (b-val b) = yes (bool , bool)
infer Γ skip = yes (unit , skip)
infer Γ (e₁ ⍮ e₂) with Γ ⊢? e₁ ∶ unit | infer Γ e₂
... | no proof₁ | _ =
  no λ { (_ , seq e₁:unit _) → proof₁ e₁:unit}
... | yes proof₁ | no proof₂ =
  no λ { (T , seq _ e₂:T) → proof₂ (T , e₂:T)}
... | yes e₁:unit | yes (T , e₂:T) =
  yes (T , seq e₁:unit e₂:T)
infer Γ (while e₁ do' e₂) with Γ ⊢? e₁ ∶ bool | Γ ⊢? e₂ ∶ unit
... | yes e₁:bool | yes e₂:unit = yes (unit , while e₁:bool e₂:unit)
... | yes e₁:bool | no proof₂ =
  no λ { (.unit , while e₁:bool e₂:unit) → proof₂ e₂:unit}
... | no proof₁ | _ =
  no λ { (.unit , while e₁:bool e₂:unit) → proof₁ e₁:bool}
infer Γ (e₁ ′ + ′ e₂) with Γ ⊢? e₁ ∶ int | Γ ⊢? e₂ ∶ int
... | yes e₁:int | yes e₂:int = yes (int , op+ e₁:int e₂:int)
... | yes e₁:int | no proof₂ =
  no λ { (.int , op+ e₁:int e₂:int) → proof₂ e₂:int}
... | no proof₁ | _ =
  no λ { (.int , op+ e₁:int e₂:int) → proof₁ e₁:int}
infer Γ (e₁ ′ ≥ ′ e₂) with Γ ⊢? e₁ ∶ int | Γ ⊢? e₂ ∶ int
... | yes e₁:int | yes e₂:int = yes (bool , op≥ e₁:int e₂:int)
... | yes e₁:int | no proof₂ =
  no λ { (.bool , op≥ e₁:int e₂:int) → proof₂ e₂:int}
... | no proof₁ | _ =
  no λ { (.bool , op≥ e₁:int e₂:int) → proof₁ e₁:int}
infer Γ (! ℓ₀) with ℓ₀ ∈? dom (proj₁ Γ)
... | yes ℓ₀∈domΓ = yes (int , deref ℓ₀∈domΓ refl)
... | no ℓ₀∉domΓ = no λ { (.int , deref ℓ∈Γ Γ[l]=intref) → ℓ₀∉domΓ ℓ∈Γ}
infer Γ (ℓ₀ := e) with Γ ⊢? e ∶ int | ℓ₀ ∈? dom (proj₁ Γ)
... | yes e:int | yes ℓ₀∈domΓ = yes (unit , assign ℓ₀∈domΓ refl e:int)
... | yes e:int | no ℓ₀∉domΓ =
  no λ { (.unit , assign p Γ[l]=intref e:int) → ℓ₀∉domΓ p}
... | no proof₁ | _ =
  no λ { (.unit , assign p Γ[l]=intref e:int) → proof₁ e:int}
infer Γ (if e₁ then e₂ else' e₃) with infer Γ e₂
... | no proof₂ =
  no λ { (T , if e₁:bool e₂:T e₃:T) → proof₂ (T , e₂:T)}
... | yes (T , e₂:T) with Γ ⊢? e₁ ∶ bool | Γ ⊢? e₃ ∶ T
... | yes e₁:bool | yes e₃:T = yes (T , if e₁:bool e₂:T e₃:T)
... | yes e₁:bool | no proof₃ =
  no λ { (T' , if e₁:bool e₂:T' e₃:T') →
  case ⊢! e₂:T e₂:T' of λ { refl → proof₃ e₃:T'} }
... | no proof₁ | _ =
  no λ { (T' , if e₁:bool e₂:T' e₃:T') → proof₁ e₁:bool}
infer Γ (fn x ∶ T ⇒ e) with infer (Γ ، x ∶ T) e
... | yes (T' , e:T') = yes (T ⟶ T' , fn e:T')
... | no ¬p = no λ { (.T ⟶ T' , fn e:T') → ¬p (T' , e:T')}
infer Γ (e₁ ` e₂) with infer Γ e₁
... | yes (T₁ ⟶ T₂ , e₁:T₁→T₂) with Γ ⊢? e₂ ∶ T₁
... | yes e₂:T₁ = yes (T₂ , app e₁:T₁→T₂ e₂:T₁)
... | no ¬e₂:T₁ = no λ { (_ , app e₁:T→T' e₂:T) →
  case ⊢! e₁:T₁→T₂ e₁:T→T' of λ { refl → ¬e₂:T₁ e₂:T} }
infer Γ (e₁ ` e₂) | yes (int , e₁:int) =
  no λ {(_ , app e₁:T→T' _) → case ⊢! e₁:int e₁:T→T' of λ ()}
infer Γ (e₁ ` e₂) | yes (bool , e₁:bool) =
  no λ {(_ , app e₁:T→T' _) → case ⊢! e₁:bool e₁:T→T' of λ ()}
infer Γ (e₁ ` e₂) | yes (unit , e₁:unit) =
  no λ {(_ , app e₁:T→T' _) → case ⊢! e₁:unit e₁:T→T' of λ ()}
infer Γ (e₁ ` e₂) | no ¬p =
  no λ { (T' , app {T = T} e₁:T→T' _) → ¬p (T ⟶ T' , e₁:T→T')}
infer Γ (var x) with x X.∈? dom (proj₂ Γ)
... | yes x∈Γ = yes (proj₂ Γ X.lookup[ x∈Γ ] x , var x∈Γ refl)
... | no x∉Γ = no λ { (_ , var x∈Γ _) → x∉Γ x∈Γ}
infer Γ let-val x ∶ T == e₁ in' e₂ end
  with Γ ⊢? e₁ ∶ T ×-dec infer (Γ ، x ∶ T) e₂
... | yes (e₁:T , T' , e₂:T') = yes (T' , let' e₁:T e₂:T')
... | no ¬p = no λ { (T' , let' e₁:T e₂:T') → ¬p (e₁:T , T' , e₂:T')}
infer Γ let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ e₁ in' e₂ end
  with Γ ، x ∶ T₁ ⟶ T₂ ، y ∶ T₁ ⊢? e₁ ∶ T₂ ×-dec
       infer (Γ ، x ∶ T₁ ⟶ T₂) e₂
... | yes (e₁:T₂ , T' , e₂:T') = yes (T' , let-rec-fn e₁:T₂ e₂:T')
... | no ¬p = no λ { (T' , let-rec-fn e₁:T₂ e₂:T') → ¬p (e₁:T₂ , T' , e₂:T')}

open 𝕋-mod

Γ ⊢? n-val n ∶ int = yes int
Γ ⊢? n-val n ∶ bool = no λ ()
Γ ⊢? n-val n ∶ unit = no λ ()
Γ ⊢? n-val n ∶ (_ ⟶ _) = no λ ()
Γ ⊢? b-val b ∶ int = no λ ()
Γ ⊢? b-val b ∶ bool = yes bool
Γ ⊢? b-val b ∶ unit = no λ ()
Γ ⊢? b-val b ∶ (_ ⟶ _) = no λ ()
Γ ⊢? e₁ ′ op ′ e₂ ∶ unit = no λ ()
Γ ⊢? e₁ ′ op ′ e₂ ∶ (_ ⟶ _) = no λ ()
Γ ⊢? e₁ ′ ≥ ′ e₂ ∶ int = no λ ()
Γ ⊢? e₁ ′ ≥ ′ e₂ ∶ bool with Γ ⊢? e₁ ∶ int ×-dec Γ ⊢? e₂ ∶ int
... | yes (e₁:int , e₂:int) = yes (op≥ e₁:int e₂:int)
... | no ¬p = no λ { (op≥ e₁:int e₂:int) → ¬p (e₁:int , e₂:int)}
Γ ⊢? e₁ ′ + ′ e₂ ∶ int with Γ ⊢? e₁ ∶ int ×-dec Γ ⊢? e₂ ∶ int
... | yes (e₁:int , e₂:int) = yes (op+ e₁:int e₂:int)
... | no ¬p = no λ { (op+ e₁:int e₂:int) → ¬p (e₁:int , e₂:int)}
Γ ⊢? e₁ ′ + ′ e₂ ∶ bool = no λ ()
Γ ⊢? if e₁ then e₂ else' e₃ ∶ T
  with Γ ⊢? e₁ ∶ bool ×-dec Γ ⊢? e₂ ∶ T ×-dec Γ ⊢? e₃ ∶ T
... | yes (e₁:bool , e₂:T , e₃:T) =  yes (if e₁:bool e₂:T e₃:T)
... | no ¬p = no λ { (if e₁:bool e₂:T e₃:T) → ¬p (e₁:bool , e₂:T , e₃:T)}
Γ ⊢? ℓ := e ∶ int = no λ ()
Γ ⊢? ℓ := e ∶ bool = no λ ()
Γ ⊢? ℓ := e ∶ unit with ℓ ∈? dom (proj₁ Γ) ×-dec Γ ⊢? e ∶ int
... | yes (ℓ∈Γ , e:int) = yes (assign ℓ∈Γ refl e:int)
... | no ¬p = no λ { (assign ℓ∈Γ _ e:int) → ¬p (ℓ∈Γ , e:int)}
Γ ⊢? ℓ := e ∶ (_ ⟶ _) = no λ ()
Γ ⊢? skip ∶ int = no λ ()
Γ ⊢? skip ∶ bool = no λ ()
Γ ⊢? skip ∶ unit = yes skip
Γ ⊢? skip ∶ (_ ⟶ _) = no λ ()
Γ ⊢? ! ℓ ∶ int with ℓ ∈? dom (proj₁ Γ)
... | yes ℓ∈Γ = yes (deref ℓ∈Γ refl)
... | no ℓ∉Γ = no λ { (deref ℓ∈Γ _) → ℓ∉Γ ℓ∈Γ}
Γ ⊢? ! ℓ ∶ bool = no λ ()
Γ ⊢? ! ℓ ∶ unit = no λ ()
Γ ⊢? ! ℓ ∶ (_ ⟶ _) = no λ ()
Γ ⊢? e₁ ⍮ e₂ ∶ T with Γ ⊢? e₁ ∶ unit ×-dec Γ ⊢? e₂ ∶ T
... | yes (e₁:unit , e₂:T) = yes (seq e₁:unit e₂:T)
... | no ¬p = no λ { (seq e₁:unit e₂:T) → ¬p (e₁:unit , e₂:T)}
Γ ⊢? while e₁ do' e₂ ∶ int = no λ ()
Γ ⊢? while e₁ do' e₂ ∶ bool = no λ ()
Γ ⊢? while e₁ do' e₂ ∶ unit with Γ ⊢? e₁ ∶ bool ×-dec Γ ⊢? e₂ ∶ unit
... | yes (e₁:bool , e₂:unit) = yes (while e₁:bool e₂:unit)
... | no ¬p = no λ { (while e₁:bool e₂:unit) → ¬p (e₁:bool , e₂:unit)}
Γ ⊢? while e₁ do' e₂ ∶ (_ ⟶ _) = no λ ()
Γ ⊢? fn x ∶ T ⇒ e ∶ int = no λ ()
Γ ⊢? fn x ∶ T ⇒ e ∶ bool = no λ ()
Γ ⊢? fn x ∶ T ⇒ e ∶ unit = no λ ()
Γ ⊢? fn x ∶ T ⇒ e ∶ (T' ⟶ T'')
  with T ≟ T' ×-dec Γ ، x ∶ T ⊢? e ∶ T''
... | yes (refl , e:T'') = yes (fn e:T'')
... | no ¬p = no λ { (fn e:T'') → ¬p (refl , e:T'')}
Γ ⊢? e₁ ` e₂ ∶ T with infer Γ e₁
... | no ¬Γ⊢e₁ = no λ { (app {T = T'}{T' = T″} e₁:T'→T″ _) →
                        ¬Γ⊢e₁ (T' ⟶ T″ , e₁:T'→T″)}
... | yes (int , e₁:int) =
  no λ { (app e₁:T'→T _) → case ⊢! e₁:int e₁:T'→T of λ ()}
... | yes (bool , e₁:bool) =
  no λ { (app e₁:T'→T _) → case ⊢! e₁:bool e₁:T'→T of λ ()}
... | yes (unit , e₁:unit) =
  no λ { (app e₁:T'→T _) → case ⊢! e₁:unit e₁:T'→T of λ ()}
... | yes (T' ⟶ T″ , e₁:T'→T″) with T″ ≟ T ×-dec Γ ⊢? e₂ ∶ T'
... | yes (refl , e₂:T') = yes (app e₁:T'→T″ e₂:T') 
... | no ¬p = no λ { (app e₁:T₁→T e₂:T₁) →
  case ⊢! e₁:T'→T″ e₁:T₁→T of λ {refl → ¬p (refl , e₂:T₁)}}
Γ ⊢? var x ∶ T with x X.∈? dom (proj₂ Γ)
... | no x∉Γ = no λ { (var x∈Γ refl) → x∉Γ x∈Γ}
... | yes x∈Γ with T ≟ (proj₂ Γ X.lookup[ x∈Γ ] x)
... | yes refl = yes (var x∈Γ refl)
... | no ¬p = no λ { (var x∈Γ refl) → ¬p refl}
Γ ⊢? let-val x ∶ T₁ == e₁ in' e₂ end ∶ T
  with Γ ⊢? e₁ ∶ T₁ ×-dec Γ ، x ∶ T₁ ⊢? e₂ ∶ T
... | yes (e₁:T₁ , e₂:T) = yes (let' e₁:T₁ e₂:T)
... | no ¬p = no λ { (let' e₁:T₁ e₂:T) → ¬p (e₁:T₁ , e₂:T)}
Γ ⊢? let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ e₁ in' e₂ end ∶ T
  with Γ ، x ∶ T₁ ⟶ T₂ ، y ∶ T₁ ⊢? e₁ ∶ T₂ ×-dec
       Γ ، x ∶ T₁ ⟶ T₂ ⊢? e₂ ∶ T
... | yes (e₁:T₂ , e₂:T) = yes (let-rec-fn e₁:T₂ e₂:T)
... | no ¬p = no λ { (let-rec-fn e₁:T₂ e₂:T) → ¬p (e₁:T₂ , e₂:T) }

⊢! int int = refl
⊢! bool bool = refl
⊢! (op+ _ _)(op+ _ _) = refl
⊢! (op≥ _ _)(op≥ _ _) = refl
⊢! (if _ e₂:T _)(if _ e₂:T' _) = ⊢! e₂:T e₂:T'
⊢! (assign _ _ _)(assign _ _ _) = refl
⊢! (deref _ _)(deref _ _) = refl
⊢! skip skip = refl
⊢! (seq _ e₂:T)(seq _ e₂:T') = ⊢! e₂:T e₂:T'
⊢! (while _ _) (while _ _) = refl
⊢! (fn {T = T} e:T')(fn e:T″) = cong (T ⟶_) $ ⊢! e:T' e:T″ 
⊢! (app e:T'→T″ _)(app e:T₁'→T₁″ _) with refl ← ⊢! e:T'→T″ e:T₁'→T₁″ = refl
⊢! (var _ refl)(var _ refl) = refl
⊢! (let' _ e₂:T)(let' _ e₂:T') = ⊢! e₂:T e₂:T'
⊢! (let-rec-fn _ e₂:T)(let-rec-fn _ e₂:T') = ⊢! e₂:T e₂:T'

type-inference = infer
decidability-of-type-checking = _⊢?_∶_
uniqueness-of-typing = ⊢!
