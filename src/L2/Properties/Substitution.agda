{-# OPTIONS --exact-split --safe  #-}
module L2.Properties.Substitution where

open import L2.Definition

open import Data.List.Relation.Unary.Any hiding (lookup)

open import Variable
open import FinitePartialFunction

private
  variable
    Γ : TypeEnv
    e e' e₁ e₂ : L₂
    x x' y y' : 𝕏
    T T' T₁ T₂ : 𝕋
    σ σ' : Subst

-- Lemma 20
substitution :
  Γ ⊢ e ∶ T →
  Γ ، x ∶ T ⊢ e' ∶ T' →
  ----------------------------------------
  Γ ⊢ sub ｛ e / x ｝ e' ∶ T'

open import Function

_⊛_ : Subst → Subst → Subst
σ ⊛ σ' = {!!}

open import Relation.Binary.PropositionalEquality

sub∘ :
  sub σ ∘ sub σ' ≡ sub (σ ⊛ σ')
sub∘ = {!!}

ren :
  Γ ، x ∶ T ⊢ e ∶ T' →
  ----------------------------------------
  Γ ، x' ∶ T ⊢ sub ｛ var x' / x ｝ e ∶ T'
ren = {!!}

open import Relation.Nullary

subs :
  Γ ⊢ e ∶ T →
  Γ ، x ∶ T ⊢ e' ∶ T' →
  ----------------------------------------
  Γ ⊢ sub ｛ e / x ｝ e' ∶ T'
subs e:T int = int
subs e:T bool = bool
subs e:T (op+ e₁:int e₂:int) = op+ (subs e:T e₁:int)(subs e:T e₂:int)
subs e:T (op≥ e₁:int e₂:int) = op≥ (subs e:T e₁:int)(subs e:T e₂:int)
subs e:T (if e₁:bool e₂:T' e₃:T') =
  if (subs e:T e₁:bool)(subs e:T e₂:T')(subs e:T e₃:T')
subs e:T (assign ℓ∈Γ refl e₁:int) = assign ℓ∈Γ refl (subs e:T e₁:int)
subs e:T (deref ℓ∈Γ refl) = deref ℓ∈Γ refl
subs e:T skip = skip
subs e:T (seq e₁:unit e₂:T') = seq (subs e:T e₁:unit)(subs e:T e₂:T')
subs e:T (while e₁:bool e₂:unit) = while (subs e:T e₁:bool)(subs e:T e₂:unit)
subs {e = e}{x = x} e:T (var {x = x'} x'∈Γ refl) with x' X.∈? dom ｛ e / x ｝
... | yes (here refl) with x' 𝕏-mod.≟ x'
subs e:T (var x'∈Γ refl) | _ | yes refl = e:T
subs e:T (var x'∈Γ refl) | _ | no x'≠x'
  with () ← x'≠x' refl
subs e:T (var (here refl) refl) | no x'∉[x]
  with () ← x'∉[x] (here refl)
subs {x = x} e:T (var {x = x'} (there x'∈Γ) refl) | no x'∉[x]
  with x' 𝕏-mod.≟ x
... | yes refl with () ← x'∉[x] (here refl)
... | no _ = var x'∈Γ refl
subs e:T (fn e':T') = {!!}
subs e:T (app e₁:T₁→T₂ e₂:T₁) = app (subs e:T e₁:T₁→T₂)(subs e:T e₂:T₁)
subs {e = e}{x = x} e:T (let' {e₂ = e₂} e₁:T₁ e₂:T₂) =
  let' (subs e:T e₁:T₁) {!!}
  where open 𝕏⇀
        x″ = fresh $ fv e₂ ∪ fv e
subs e:T (let-rec-fn e':T' e':T'') = {!!}

substitution = subs
