{-# OPTIONS --exact-split --safe  #-}
module L2.Definition.Substitution where

open import L2.Definition.Base

open import FinitePartialFunction
open import FiniteSubset
open import Variable

module 𝕏⇀ where
  open DecEquality 𝕏-mod._≟_ public
open 𝕏⇀

fv : (e : L₂) → Subset 𝕏
fv (n-val n) = ∅
fv (b-val b) = ∅
fv (e₁ ′ op ′ e₂) = fv e₁ ∪ fv e₂
fv (if e₁ then e₂ else' e₃) = fv e₁ ∪ fv e₂ ∪ fv e₃
fv (ℓ₀ := e) = fv e
fv skip = ∅
fv (! ℓ₀) = ∅
fv (e₁ ⍮ e₂) = fv e₁ ∪ fv e₂
fv (while e₁ do' e₂) = fv e₁ ∪ fv e₂
fv (fn x ∶ T ⇒ e) = fv e - ｛ x ｝
fv (e₁ ` e₂) = fv e₁ ∪ fv e₂
fv (var x) = ｛ x ｝
fv let-val x ∶ T == e₁ in' e₂ end = fv e₁ ∪ (fv e₂ - ｛ x ｝)
fv let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ e₁ in' e₂ end =
  (fv e₁ - ｛ y ｝) ∪ fv e₂ - ｛ x ｝ 

bv : (e : L₂) → Subset 𝕏
bv (n-val n) = ∅
bv (b-val b) = ∅
bv (e₁ ′ op ′ e₂) = bv e₁ ∪ bv e₂
bv (if e₁ then e₂ else' e₃) = bv e₁ ∪ bv e₂ ∪ bv e₃
bv (ℓ₀ := e) = bv e
bv skip = ∅
bv (! ℓ₀) = ∅
bv (e₁ ⍮ e₂) = bv e₁ ∪ bv e₂
bv (while e₁ do' e₂) = bv e₁ ∪ bv e₂
bv (fn x ∶ T ⇒ e) = bv e ∪ ｛ x ｝
bv (e₁ ` e₂) = bv e₁ ∪ bv e₂
bv (var x) = ∅
bv let-val x ∶ T == e₁ in' e₂ end = bv e₁ ∪ bv e₂ ∪ ｛ x ｝
bv let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ e₁ in' e₂ end =
  bv e₁ ∪ bv e₂ ∪ ｛ x ｝ ∪ ｛ y ｝ 

open import FinitePartialFunction

Subst = 𝕏 ⇀fin L₂

open import Data.List as L using (List)
open import Data.List.Membership.Propositional using (mapWith∈)
module X where
  open WithDecidable 𝕏-mod._≟_ public
  open import Data.List.Membership.DecPropositional 𝕏-mod._≟_ public

open import Function

allvar : (σ : 𝕏 ⇀fin L₂) → List 𝕏
allvar σ = dom σ ∪ ⋃ (L.map fv $ ran σ)

open import Relation.Nullary

sub' : (σ : Subst)(e : L₂).(p : ∀{x}(x∈e : x ∈ bv e) → x ∉ allvar σ) → L₂
sub' σ (n-val n) _ = n-val n
sub' σ (b-val b) _ = b-val b
sub' σ (e₁ ′ op ′ e₂) p = sub' σ e₁ {!!} ′ op ′ sub' σ e₂ {!!}
sub' σ (if e₁ then e₂ else' e₃) p =
  if sub' σ e₁ {!!} then sub' σ e₂ {!!} else' sub' σ e₃ {!!}
sub' σ (ℓ₀ := e) p = ℓ₀ := sub' σ e p
sub' σ skip _ = skip
sub' σ (! ℓ₀) _ = ! ℓ₀
sub' σ (e₁ ⍮ e₂) p = sub' σ e₁ {!!} ⍮ sub' σ e₂ {!!}
sub' σ (while e₁ do' e₂) p = while sub' σ e₁ {!!} do' sub' σ e₂ {!!}
sub' σ (fn x ∶ T ⇒ e) p = fn x ∶ T ⇒ sub' σ e {!!}
sub' σ (e₁ ` e₂) p = sub' σ e₁ {!!} ` sub' σ e₂ {!!}
sub' σ (var x) _ with x X.∈? dom σ
... | yes x∈dom-σ = σ X.lookup[ x∈dom-σ ] x
... | no _ = var x
sub' σ let-val x ∶ T == e₁ in' e₂ end p =
  let-val x ∶ T == sub' σ e₁ {!!} in' sub' σ e₂ {!!} end
sub' σ let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ e₁ in' e₂ end p =
  let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ sub' σ e₁ {!!}
  in' sub' σ e₂ {!!} end

sub : (σ : Subst)(e : L₂) → L₂
sub = {!!}

module SubTest where
  x y z : 𝕏
  x = var 0
  y = var 1
  z = var 2

  e₁ = fn y ∶ int ⇒ (var z ` var y)
  e₂ = fn y ∶ int ⇒ (var x ` var y)
  e₃ = fn x ∶ int ⇒ (var x ` var x)

  σ₁ σ₂ σ₃ : Subst
  σ₁ = ｛ fn x ∶ int ⇒ var y / z ｝
  σ₂ = ｛ fn x ∶ int ⇒ var x / x ｝
  σ₃ = σ₂

  open import Data.Product
  open import Relation.Binary.PropositionalEquality
  
  test₁ : ∃ λ y' → sub σ₁ e₁ ≡ fn y' ∶ int ⇒ (fn x ∶ int ⇒ var y) ` var y'
  test₁ = x , {!!}

  test₂ : ∃ λ y' → sub σ₂ e₂ ≡ fn y' ∶ int ⇒ (fn x ∶ int ⇒ var x) ` var y'
  test₂ = y , {!!}

  test₃ : ∃ λ x' → sub σ₃ e₃ ≡ fn x' ∶ int ⇒ var x' ` var x'
  test₃ = x , {!!}
