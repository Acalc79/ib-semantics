{-# OPTIONS --exact-split --safe  #-}
module L2.Definition.Base where

open import Common hiding (Config; ℓ) public

infixr 115 _⟶_
data 𝕋 : Set where
  int bool unit : 𝕋
  _⟶_ : (T₁ T₂ : 𝕋) → 𝕋

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

module 𝕋-mod where
  _≟_ : (T T' : 𝕋) → Dec (T ≡ T')
  int ≟ int = yes refl
  int ≟ bool = no λ ()
  int ≟ unit = no λ ()
  int ≟ (T' ⟶ T'') = no λ ()
  bool ≟ int = no λ ()
  bool ≟ bool = yes refl
  bool ≟ unit = no λ ()
  bool ≟ (T' ⟶ T'') = no λ ()
  unit ≟ int = no λ ()
  unit ≟ bool = no λ ()
  unit ≟ unit = yes refl
  unit ≟ (T' ⟶ T'') = no λ ()
  (_ ⟶ _) ≟ int = no λ ()
  (_ ⟶ _) ≟ bool = no λ ()
  (_ ⟶ _) ≟ unit = no λ ()
  (T₁ ⟶ T₂) ≟ (T₁' ⟶ T₂') with T₁ ≟ T₁' | T₂ ≟ T₂'
  ... | yes refl | yes refl = yes refl
  ... | yes refl | no T₂≠T₂' = no λ {refl → T₂≠T₂' refl}
  ... | no T₁≠T₁' | _ = no λ {refl → T₁≠T₁' refl}

open import Data.Integer using (ℤ)
open import Data.Bool using (Bool; true; false)
import Variable
open Variable
open Variable using (𝕏; var) public

infixl 110 _′_′_ _⍮_
infix 110 _:=_ !_
infixr 109 fn_∶_⇒_
infixl 110 _`_ if_then_else'_ while_do'_
infix 110 let-val_∶_==_in'_end
infix 110 let-val-rec_∶_—>_==fn_⇒_in'_end
data L₂ : Set where
  n-val : (n : ℤ) → L₂
  b-val : (b : Bool) → L₂
  _′_′_ : (e₁ : L₂)(op : Op)(e₂ : L₂) → L₂
  if_then_else'_ : (e₁ e₂ e₃ : L₂) → L₂
  _:=_ : (ℓ : 𝕃)(e : L₂) → L₂
  skip : L₂
  !_ : (ℓ : 𝕃) → L₂
  _⍮_ : (e₁ e₂ : L₂) → L₂
  while_do'_ : (e₁ e₂ : L₂) → L₂
  fn_∶_⇒_ : (x : 𝕏)(T : 𝕋)(e : L₂) → L₂
  _`_ : (e₁ e₂ : L₂) → L₂
  var : (x : 𝕏) → L₂
  let-val_∶_==_in'_end : (x : 𝕏)(T : 𝕋)(e₁ e₂ : L₂) → L₂
  let-val-rec_∶_—>_==fn_⇒_in'_end : (x : 𝕏)(T₁ T₂ : 𝕋)(y : 𝕏)(e₁ e₂ : L₂) → L₂

{-# DISPLAY L₂.var x = x #-}
{-# DISPLAY n-val n = n #-}
{-# DISPLAY b-val b = b #-}
