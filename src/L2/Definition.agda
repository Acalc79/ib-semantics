{-# OPTIONS --exact-split --safe  #-}
module L2.Definition where

open import Common hiding (Config; ℓ) public

infixr 115 _⟶_
data 𝕋 : Set where
  int bool unit : 𝕋
  _⟶_ : (T₁ T₂ : 𝕋) → 𝕋

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

module 𝕋-mod where
  _≟_ : (T T' : 𝕋) → Dec (T ≡ T')
  int ≟ int = yes refl
  int ≟ bool = no λ ()
  int ≟ unit = no λ ()
  int ≟ (T' ⟶ T'') = no λ ()
  bool ≟ int = no λ ()
  bool ≟ bool = yes refl
  bool ≟ unit = no λ ()
  bool ≟ (T' ⟶ T'') = no λ ()
  unit ≟ int = no λ ()
  unit ≟ bool = no λ ()
  unit ≟ unit = yes refl
  unit ≟ (T' ⟶ T'') = no λ ()
  (_ ⟶ _) ≟ int = no λ ()
  (_ ⟶ _) ≟ bool = no λ ()
  (_ ⟶ _) ≟ unit = no λ ()
  (T₁ ⟶ T₂) ≟ (T₁' ⟶ T₂') with T₁ ≟ T₁' | T₂ ≟ T₂'
  ... | yes refl | yes refl = yes refl
  ... | yes refl | no T₂≠T₂' = no λ {refl → T₂≠T₂' refl}
  ... | no T₁≠T₁' | _ = no λ {refl → T₁≠T₁' refl}

open import Data.Integer using (ℤ)
open import Data.Bool using (Bool; true; false)
import Variable
open Variable
open Variable using (𝕏; var) public

infixl 110 _′_′_ _⍮_
infix 110 _:=_ !_
infixr 109 fn_∶_⇒_
infixl 110 _`_ if_then_else'_ while_do'_
infix 110 let-val_∶_==_in'_end
infix 110 let-val-rec_∶_—>_==fn_⇒_in'_end
data L₂ : Set where
  n-val : (n : ℤ) → L₂
  b-val : (b : Bool) → L₂
  _′_′_ : (e₁ : L₂)(op : Op)(e₂ : L₂) → L₂
  if_then_else'_ : (e₁ e₂ e₃ : L₂) → L₂
  _:=_ : (ℓ : 𝕃)(e : L₂) → L₂
  skip : L₂
  !_ : (ℓ : 𝕃) → L₂
  _⍮_ : (e₁ e₂ : L₂) → L₂
  while_do'_ : (e₁ e₂ : L₂) → L₂
  fn_∶_⇒_ : (x : 𝕏)(T : 𝕋)(e : L₂) → L₂
  _`_ : (e₁ e₂ : L₂) → L₂
  var : (x : 𝕏) → L₂
  let-val_∶_==_in'_end : (x : 𝕏)(T : 𝕋)(e₁ e₂ : L₂) → L₂
  let-val-rec_∶_—>_==fn_⇒_in'_end : (x : 𝕏)(T₁ T₂ : 𝕋)(y : 𝕏)(e₁ e₂ : L₂) → L₂

{-# DISPLAY L₂.var x = x #-}
{-# DISPLAY n-val n = n #-}
{-# DISPLAY b-val b = b #-}
-- {-# DISPLAY _`_ a b = a b #-}


private
  variable n n₁ n₂ : ℤ
           b : Bool
           s s' : Store
           e e' e₁ e₁' e₂ e₂' e₃ v : L₂
           op : Op
           ℓ : 𝕃
           x x' y y' : 𝕏
           T T' T₁ T₂ : 𝕋

data value : L₂ → Set where
  n-val : value (n-val n)
  b-val : value (b-val b)
  skip : value skip
  fn-val : value (fn x ∶ T ⇒ e)

Config = Common.Config L₂

open import Data.Integer as ℤ
open import Relation.Binary.PropositionalEquality
open import Relation.Nullary.Decidable
open WithDecidable 𝕃-mod._≟_

data _⇝_ : Config → Config → Set where

  op+ :
    n ≡ n₁ ℤ.+ n₂
    → --------------------------------------------------------------
    〈 n-val n₁ ′ + ′ n-val n₂ ، s 〉 ⇝ 〈 n-val n ، s 〉

  op≥ :
    b ≡ ⌊ n₂ ℤ.≤? n₁ ⌋
    → --------------------------------------------------------------
    〈 n-val n₁ ′ ≥ ′ n-val n₂ ، s 〉 ⇝ 〈 b-val b ، s 〉

  op1 :
    〈 e₁ ، s 〉 ⇝ 〈 e₁' ، s' 〉
    → --------------------------------------------------------------
    〈 e₁ ′ op ′ e₂ ، s 〉 ⇝ 〈 e₁' ′ op ′ e₂ ، s' 〉

  op2 : ∀{e₁ op e₂ s e₂' s'}→
    value e₁ →
    〈 e₂ ، s 〉 ⇝ 〈 e₂' ، s' 〉
    → --------------------------------------------------------------
    〈 e₁ ′ op ′ e₂ ، s 〉 ⇝ 〈 e₁ ′ op ′ e₂' ، s' 〉

  deref :
    (p : ℓ ∈ dom s) →
    s lookup[ p ] ℓ ≡ n →
    ------------------------------------------------------------
    〈 ! ℓ ، s 〉 ⇝ 〈 n-val n ، s 〉
  
  assign1 :
    ℓ ∈ dom s →
    -----------------------------------------------
    〈 ℓ := n-val n ، s 〉 ⇝ 〈 skip ، s +[ ℓ ↦ n ] 〉

  assign2 :
    〈 e ، s 〉 ⇝ 〈 e' ، s' 〉
    → ------------------------------
    〈 ℓ := e ، s 〉 ⇝ 〈 ℓ := e' ، s' 〉

  seq1 :
    ------------------------------
    〈 skip ⍮ e₂ ، s 〉 ⇝ 〈 e₂ ، s 〉

  seq2 :
    〈 e₁ ، s 〉 ⇝ 〈 e₁' ، s' 〉
    → ------------------------------
    〈 e₁ ⍮ e₂ ، s 〉 ⇝ 〈 e₁' ⍮ e₂ ، s' 〉

  if1 :
    -------------------------------------------
    〈 if (b-val true) then e₂ else' e₃ ، s 〉 ⇝ 〈 e₂ ، s 〉

  if2 :
    -------------------------------------------
    〈 if (b-val false) then e₂ else' e₃ ، s 〉 ⇝ 〈 e₃ ، s 〉

  if3 :
    〈 e₁ ، s 〉 ⇝ 〈 e₁' ، s' 〉
    → ---------------------------------------------------------------
    〈 if e₁ then e₂ else' e₃ ، s 〉 ⇝ 〈 if e₁' then e₂ else' e₃ ، s' 〉

  while :
    〈 while e₁ do' e₂ ، s 〉
    ⇝
    〈 if e₁ then e₂ ⍮ (while e₁ do' e₂) else' skip ، s 〉

  app1 :
    〈 e₁ ، s 〉 ⇝ 〈 e₁' ، s' 〉
    → --------------------------------------------------------------
    〈 e₁ ` e₂ ، s 〉 ⇝ 〈 e₁' ` e₂ ، s' 〉

  app2 :
    value e₁ →
    〈 e₂ ، s 〉 ⇝ 〈 e₂' ، s' 〉
    → ------------------------------
    〈 e₁ ` e₂ ، s 〉 ⇝ 〈 e₁ ` e₂' ، s' 〉

  fn :
    value v
    → -----------------------------------------------------
    〈 (fn x ∶ T ⇒ e) ` v ، s 〉 ⇝ 〈 sub ｛ v / x ｝ e ، s 〉

  let1 :
    〈 e₁ ، s 〉 ⇝ 〈 e₁' ، s' 〉
    → --------------------------------------------------------------
    〈 let-val x ∶ T == e₁ in' e₂ end ، s 〉 ⇝
    〈 let-val x ∶ T == e₁' in' e₂ end ، s' 〉

  let2 :
    value v
    → --------------------------------------------------------------
    〈 let-val x ∶ T == v in' e₂ end ، s 〉 ⇝ 〈 sub ｛ v / x ｝ e₂ ، s 〉

  letrecfn :
    let val-rec = let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ e₁ in' e₂ end
    in --------------------------------------------------------------
    〈 val-rec ، s 〉 ⇝ 〈 sub ｛ fn y ∶ T₁ ⇒ val-rec / x ｝ e₂ ، s 〉

open import Data.Product

TypeEnv = 𝕃 ⇀fin 𝕋𝕃 × 𝕏 ⇀fin 𝕋

infixl 30 _،_∶_
_،_∶_ : (Γ : TypeEnv)(x : 𝕏)(T : 𝕋) → TypeEnv
Γ ، x ∶ T = (proj₁ Γ , proj₂ Γ X.+[ x ↦ T ])

data _⊢_∶_ (Γ : TypeEnv) : (e : L₂)(T : 𝕋) → Set where
  int :
    ------------------------------------------
    Γ ⊢ n-val n ∶ int

  bool :
    ------------------------------------------
    Γ ⊢ b-val b ∶ bool

  op+ :
    (e₁:int : Γ ⊢ e₁ ∶ int)(e₂:int : Γ ⊢ e₂ ∶ int)
    → ------------------------------------------
    Γ ⊢ e₁ ′ + ′ e₂ ∶ int

  op≥ :
    (e₁:int : Γ ⊢ e₁ ∶ int)(e₂:int : Γ ⊢ e₂ ∶ int)
    → ------------------------------------------
    Γ ⊢ e₁ ′ ≥ ′ e₂ ∶ bool

  if :
    (e₁:bool : Γ ⊢ e₁ ∶ bool)(e₂:T : Γ ⊢ e₂ ∶ T)(e₃:T : Γ ⊢ e₃ ∶ T)
    → ---------------------------------------------------
    Γ ⊢ if e₁ then e₂ else' e₃ ∶ T

  assign :
    (ℓ∈Γ : ℓ ∈ dom (proj₁ Γ))
    (Γ[l]=intref : proj₁ Γ lookup[ ℓ∈Γ ] ℓ ≡ intref)
    (e:int : Γ ⊢ e ∶ int)
    → -------------------------------------------------------------------------
    Γ ⊢ ℓ := e ∶ unit

  deref :
    (ℓ∈Γ : ℓ ∈ dom (proj₁ Γ))
    (Γ[l]=intref : proj₁ Γ lookup[ ℓ∈Γ ] ℓ ≡ intref)
    → ------------------------------------------
    Γ ⊢ ! ℓ ∶ int

  skip : --------------------
    Γ ⊢ skip ∶ unit

  seq :
    (e₁:unit : Γ ⊢ e₁ ∶ unit)(e₂:T : Γ ⊢ e₂ ∶ T)
    → ------------------------------------------
    Γ ⊢ e₁ ⍮ e₂ ∶ T

  while :
    (e₁:bool : Γ ⊢ e₁ ∶ bool)(e₂:unit : Γ ⊢ e₂ ∶ unit)
    → ------------------------------------------
    Γ ⊢ while e₁ do' e₂ ∶ unit

  var :
    (x∈Γ : x ∈ dom (proj₂ Γ)) →
    (Γ[x]=T : proj₂ Γ X.lookup[ x∈Γ ] x ≡ T)
    → ------------------------------------------
    Γ ⊢ var x ∶ T

  fn :
    (e:T' : Γ ، x ∶ T ⊢ e ∶ T')
    → -----------------------------------------------
    Γ ⊢ fn x ∶ T ⇒ e ∶ T ⟶ T'

  app :
    (e₁:T→T' : Γ ⊢ e₁ ∶ T ⟶ T')(e₂:T : Γ ⊢ e₂ ∶ T)
    → -----------------------------------------------
    Γ ⊢ e₁ ` e₂ ∶ T'

  let' :
    (e₁:T : Γ ⊢ e₁ ∶ T)
    (e₂:T' : Γ ، x ∶ T ⊢ e₂ ∶ T')
    → -----------------------------------------------
    Γ ⊢ let-val x ∶ T == e₁ in' e₂ end ∶ T'

  let-rec-fn :
    (e₁:T₂ : Γ ، x ∶ T₁ ⟶ T₂ ، y ∶ T₁ ⊢ e₁ ∶ T₂)
    (e₂:T : Γ ، x ∶ T₁ ⟶ T₂ ⊢ e₂ ∶ T)
    → ---------------------------------------------------------------------
    Γ ⊢ let-val-rec x ∶ T₁ —> T₂ ==fn y ⇒ e₁ in' e₂ end ∶ T
