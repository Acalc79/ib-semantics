{-# OPTIONS --exact-split --safe  #-}
module Data.List.Relation.Unary.Any.Properties.Extra where

open import Data.List.Relation.Unary.Any
open import Data.List.Relation.Unary.Any.Properties

open import Level using (Level)
open import Function
open import Relation.Binary.PropositionalEquality
open import Data.List as List
open import Data.List.Properties
open import Data.Fin
open import Data.Fin.Properties

private
  variable
    a b c p q r ℓ : Level
    A : Set a
    B : Set b
    C : Set c

index-injective : ∀{a : A}{xs}
  → ----------------------------------------
  Injective _≡_ _≡_ (index {P = a ≡_}{xs})
index-injective {x = here refl}{here refl} _ = refl
index-injective {x = there _}{there _} i₁≡i₂ =
  cong there $ index-injective $ suc-injective i₁≡i₂

module _ {f : A → B}{P : B → Set p} where

  map⁺-injective : ∀{xs}
    → --------------------------------------------------
    Injective _≡_ _≡_ (map⁺ {f = f}{P = P}{xs})
  map⁺-injective {x = here _}{here _} refl = refl
  map⁺-injective {x = there p₁}{there p₂} ≡ =
    cong there $ map⁺-injective $ there-injective ≡

  map⁻-injective : ∀{xs}
    → --------------------------------------------------
    Injective _≡_ _≡_ (map⁻ {f = f}{P = P}{xs})
  map⁻-injective {xs = _ ∷ _}{x = here _}{here _} refl = refl
  map⁻-injective {xs = _ ∷ _}{x = there p₁}{there p₂} ≡ =
    cong there $ map⁻-injective $ there-injective ≡

  open import Relation.Binary.HeterogeneousEquality as Het
    hiding (subst; sym)
  import Data.Nat.Properties as NatP

  index-map⁺ : ∀{xs}(p : Any (P ∘ f) xs)
    → ---------------------------------------------------------------------
    index (map⁺ {P = P} p) ≅ index p
  index-map⁺ {xs = _ ∷ xs}(here px) =
    Het.cong (λ n → zero {n}) $ ≡-to-≅ $ length-map f xs
  index-map⁺ {xs = _ ∷ xs} (there p) =
    Het.cong₂ (λ n fn → suc {n} fn)
      (≡-to-≅ $ length-map f xs)
      (index-map⁺ p)

  index-map⁻ : ∀{xs}(p : Any P (List.map f xs))
    → ---------------------------------------------------------------------
    index (map⁻ {P = P} p) ≅ index p
  index-map⁻ {xs = x ∷ xs} (here px) =
    Het.cong (λ n → zero {n}) $ ≡-to-≅ $ sym $ length-map f xs
  index-map⁻ {xs = _ ∷ xs} (there p) =
    Het.cong₂ (λ n fn → suc {n} fn)
      (≡-to-≅ $ sym $ length-map f xs)
      (index-map⁻ p)

open import Relation.Unary as U
open import Relation.Nullary using (does)
open import Data.Bool

module _ {P : A → Set p} {Q : A → Set q} (Q? : U.Decidable Q) where

  filter⁻-injective : ∀{xs}
    → ---------------------------------------------
    Injective _≡_ _≡_ (filter⁻ {P = P} Q? {xs})
  filter⁻-injective {x ∷ xs} {x = p₁} {p₂} ≡ with does (Q? x)
  ... | false = filter⁻-injective $ there-injective ≡
  filter⁻-injective {x = here _}{here _} refl | true = refl
  filter⁻-injective {x = there _}{there _} ≡ | true =
    cong there $ filter⁻-injective $ there-injective ≡

module _ {P : A → Set p} where

  concat⁻-injective : ∀{xss}
    → --------------------------------------------------
    Injective _≡_ _≡_ (concat⁻ {P = P} xss)
  concat⁻-injective {[] ∷ _} ≡ = concat⁻-injective $ there-injective ≡ 
  concat⁻-injective {(_ ∷ _) ∷ _} {here _}{here _} refl = refl
  concat⁻-injective {(_ ∷ xs) ∷ xss} {here _} {there p₂} ≡
    with concat⁻ (xs ∷ xss) p₂
  concat⁻-injective () | here _
  concat⁻-injective () | there _
  concat⁻-injective {(_ ∷ xs) ∷ xss} {there p₁}{here _} ≡
    with concat⁻ (xs ∷ xss) p₁
  concat⁻-injective () | here _
  concat⁻-injective () | there _
  concat⁻-injective {(x ∷ xs) ∷ xss} {there p₁}{there p₂} ≡
    with concat⁻ (xs ∷ xss) p₁ | concat⁻ (xs ∷ xss) p₂
       | inspect (concat⁻ (xs ∷ xss)) p₁ | inspect (concat⁻ (xs ∷ xss)) p₂
  concat⁻-injective {(_ ∷ xs) ∷ xss} refl
    | here _ | here _ | [ ≡₁ ] | [ ≡₂ ] =
    cong there $ concat⁻-injective {xss = xs ∷ xss} $ trans ≡₁ (sym ≡₂)
  concat⁻-injective {(_ ∷ xs) ∷ xss} refl
    | there _ | there _ | [ ≡₁ ] | [ ≡₂ ] =
    cong there $ concat⁻-injective {xss = xs ∷ xss} $ trans ≡₁ (sym ≡₂)
