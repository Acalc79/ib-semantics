{-# OPTIONS --exact-split --safe  #-}
module Variable where

open import Data.Nat as ℕ hiding (_≟_; _≤_; _<_)

record 𝕏 : Set where
  constructor var
  field
    unvar : ℕ

{-# DISPLAY var 0 = 'x' #-}
{-# DISPLAY var 1 = 'y' #-}
{-# DISPLAY var 2 = 'z' #-}
{-# DISPLAY var 3 = 'w' #-}

open 𝕏

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

module 𝕏-mod where
  _≟_ : (x y : 𝕏) → Dec (x ≡ y)
  var x ≟ var y with x ℕ.≟ y
  ... | yes refl = yes refl
  ... | no ¬x≡y = no λ { refl → ¬x≡y refl}

  _≤_ _<_ : (x y : 𝕏) → Set
  var n ≤ var m = n ℕ.≤ m
  var n < var m = n ℕ.< m
  
open 𝕏-mod

open import Data.Sum hiding (map)
open import Data.Empty
open import Data.List
open import Data.List.Membership.Propositional
open import Data.List.Membership.Propositional.Properties
open import Data.List.Membership.DecPropositional 𝕏-mod._≟_
  renaming (_∈?_ to _𝕏∈?_) using ()
open import Data.List.Membership.DecPropositional ℕ._≟_ as ℕ∈
  renaming (_∈?_ to _ℕ∈?_) using ()
open import Data.List.Relation.Unary.Any as Any hiding (map)
-- open import Data.List.Relation.Unary.Any.Properties
open import Data.List.Extrema.Nat
open import Data.Nat.Properties
open import Function
open import Function.Reasoning

private
  module Private where
    go-fresh : (ls : List ℕ)(x sofar : ℕ).(p : sofar ∉ ls) → ℕ
    go-fresh ls 0 sofar p = sofar
    go-fresh ls (suc x) sofar p with x ℕ∈? ls
    ... | yes x∈xs = go-fresh ls x sofar p
    ... | no x∉xs = go-fresh ls x x x∉xs

    go-fresh∉ls : (ls : List ℕ)(x sofar : ℕ)(p : sofar ∉ ls)
      → ---------------------------------------------
      go-fresh ls x sofar p ∉ ls
    go-fresh∉ls ls zero _ p = p
    go-fresh∉ls ls (suc x) sofar p with x ℕ∈? ls
    ... | yes _ = go-fresh∉ls ls x sofar p
    ... | no x∉xs = go-fresh∉ls ls x x x∉xs

    -- go-fresh-minimum : (ls : List ℕ)(n x : ℕ)(p : x ∉ ls)
    --   (q : n ℕ.< go-fresh ls x x p)
    --   → ----------------------------------------
    --   n ∈ ls
    -- go-fresh-minimum ls n (suc x) p q with x ℕ∈? ls
    -- ... | yes x∈ls = {!!}
    -- ... | no x∉ls = {!!}

    s-max∉ls : (ls : List ℕ) → suc (max 0 ls) ∉ ls
    s-max∉ls ls p = p                           ∶ s-max ∈ ls
                |> Any.map (λ { refl → ≤-refl}) ∶ Any (s-max ℕ.≤_) ls
                |> v≤max⁺ 0 ls ∘ inj₂           ∶ max 0 ls ℕ.< max 0 ls
                |> <-irrefl refl                ∶ ⊥
      where s-max = suc $ max 0 ls

open Private

fresh : (xs : List 𝕏) → 𝕏
fresh xs = var $ go-fresh ls s-max s-max (s-max∉ls ls)
  where ls = map unvar xs
        s-max = suc $ max 0 ls

fresh-is-fresh : (xs : List 𝕏) → fresh xs ∉ xs
fresh-is-fresh xs fresh-xs∈xs =
     fresh-xs∈xs                              ∶ fresh xs ∈ xs
  |> ∈-map⁺ unvar                             ∶ go-fresh ls s-max s-max _ ∈ ls
  |> go-fresh∉ls ls s-max s-max (s-max∉ls ls) ∶ ⊥
  where ls = map unvar xs
        s-max = suc $ max 0 ls

-- open import Data.List.Properties

-- fresh-minimum : (xs : List 𝕏)(x : 𝕏)
--   (p : x < fresh xs)
--   → ----------------------------------------
--   x ∈ xs
-- fresh-minimum xs (var n) p =
--      go-fresh-minimum ls n s-max (s-max∉ls ls) p ∶ n ∈ ls ∶ n ∈ ls
--   |> ∈-map⁺ var                                           ∶ var n ∈ map var ls
--   |> subst (var n ∈_) (begin map var (map unvar xs) ≡⟨ sym $ map-compose xs ⟩
--                              map id xs              ≡⟨ map-id xs ⟩
--                              xs ∎)                        ∶ var n ∈ xs
--   where ls = map unvar xs
--         s-max = suc $ max 0 ls
--         open ≡-Reasoning
