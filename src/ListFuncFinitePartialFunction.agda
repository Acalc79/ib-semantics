{-# OPTIONS --exact-split --safe  #-}
module FinitePartialFunction where

open import Level
open import Data.List hiding (lookup; [_]; tail)
open import Data.List.Membership.Propositional using (_∈_) public
open import Data.Maybe hiding (map)
open import Data.Product hiding (map)
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality
open import Function

private
  variable a b : Level
           A : Set a
           B : (a : A) → Set b

  module Private where
    record _⇀_ (A : Set a)(B : (a : A) → Set b) : Set (a ⊔ b) where
      constructor [_،_،_]
      field
        to-list : List (Σ A B)
        -- to-func : (a : A) → Maybe (B a)
        -- prop : ∀{a b} → (a , b) ∈ to-list ⇔ to-func a ≡ just b

open Private
open _⇀_

_⇀fin_ : (A : Set a)(B : Set b) → Set _
A ⇀fin B = A ⇀ λ _ → B
_⇀dep_ = _⇀_

⦃⦄ : A ⇀ B
⦃⦄ = [ [] ، (λ _ → nothing) ، mk⇔ (λ ()) (λ ()) ]

-- pattern ｛_/_｝ b a = [ (a , b) ∷ [] , ? ]

dom : A ⇀ B → List A
dom = map proj₁ ∘ to-list

ran : {B : Set a} → A ⇀fin B → List B
ran = map proj₂ ∘ to-list

open import Data.Bool hiding (_≟_)
open import Relation.Nullary
open import Relation.Nullary.Decidable
open import Data.List.Relation.Unary.Any hiding (lookup)

_+[_↦_] : (f : A ⇀ B)(a : A)(b : B a)⦃ dec : {a b : A} → Dec (a ≡ b) ⦄ → A ⇀ B
(f +[ a ↦ b ]) ⦃ dec ⦄ =
  [ (a , b) ∷ to-list f
  ، (λ a' → case (a ≟ a') of λ
            { (no _) → to-func f a'
            ; (yes refl) → just b
            })
  ، mk⇔ (λ { (here refl) → case (a ≟ a) of λ x → {!!} ; (there x) → {!Equivalence.f (prop f) x!}})
        (λ x → {!!})
  ]
  where _≟_ = λ a b → dec {a}{b}

open import Data.List.Relation.Binary.Subset.Propositional

dom⊆dom+ :
  (f : A ⇀ B)
  (a : A)(b : B a)
  ⦃ _ : {a b : A} → Dec (a ≡ b) ⦄
  → ------------------------------------
  dom f ⊆ dom (f +[ a ↦ b ])
dom⊆dom+ _ _ _ = there

∈dom+⇒dom :
  (f : A ⇀ B)
  (a : A)(b : B a)
  (a∈dom : a ∈ dom f)
  ⦃ _ : {a b : A} → Dec (a ≡ b) ⦄
  → ------------------------------------
  dom (f +[ a ↦ b ]) ⊆ dom f
∈dom+⇒dom _ _ _ a∈dom (here refl) = a∈dom
∈dom+⇒dom _ _ _ _ (there x∈dom) = x∈dom

open import Data.List.Relation.Unary.Any hiding (lookup)
open import Data.Bool

module DecidableA (_≟_ : (a b : A) → Dec (a ≡ b)) where
  lookup : (f : A ⇀ B)(a : A).(p : a ∈ dom f) → B a
  lookup f a p with to-func f a | inspect (to-func f) a
  ... | nothing | [ fa=nothing ] = {!Equivalence.f (prop f)!}
  ... | just b | [ fa=just-b ] = {!!}
  --  [ (a' , b) ∷ ls , ? ] a p with a ≟ a'
  -- ... | yes refl = b
  -- ... | no ¬p = lookup [ ls ] a (tail ¬p p)

  syntax lookup s ℓ p = s lookup[ p ] ℓ

-- non-dependent version suffices for now
_∘[_]_ : {B : Set a}{C : Set b}
  (f : A ⇀fin B)
  (_≟_ : (a b : B) → Dec (a ≡ b))
  (g : B ⇀fin C)
  → --------------------------------
  A ⇀fin C
f ∘[ _≟_ ] g = {!!}

