{-# OPTIONS --exact-split --safe  #-}
module L1.Definition where

open import Common hiding (Config) public
open import Data.Bool as 𝔹 hiding (_≟_)

infixl 110 _′_′_ if_then_else'_ _:=_ !_ _⍮_ while_do'_
data L₁ : Set where
  n-val : (n : ℤ) → L₁
  b-val : (b : Bool) → L₁
  _′_′_ : (e₁ : L₁)(op : Op)(e₂ : L₁) → L₁
  if_then_else'_ : (e₁ e₂ e₃ : L₁) → L₁
  _:=_ : (ℓ : 𝕃)(e : L₁) → L₁
  skip : L₁
  !_ : (ℓ : 𝕃) → L₁
  _⍮_ : (e₁ e₂ : L₁) → L₁
  while_do'_ : (e₁ e₂ : L₁) → L₁

data value : L₁ → Set where
  n-val : (n : ℤ) → value (n-val n)
  b-val : (b : Bool) → value (b-val b)
  skip : value skip

Config = Common.Config L₁

open import Data.Integer as ℤ
open import Relation.Binary.PropositionalEquality
open import Relation.Nullary.Decidable

open import FinitePartialFunction
open DecidableA 𝕃-mod._≟_

data _⇝_ : Config → Config → Set where

  op+ : ∀ n₁ n₂ n s →
    n ≡ n₁ ℤ.+ n₂
    → --------------------------------------------------------------
    〈 n-val n₁ ′ + ′ n-val n₂ ، s 〉 ⇝ 〈 n-val n ، s 〉

  op≥ : ∀ n₁ n₂ b s →
    b ≡ ⌊ n₂ ℤ.≤? n₁ ⌋
    → --------------------------------------------------------------
    〈 n-val n₁ ′ ≥ ′ n-val n₂ ، s 〉 ⇝ 〈 b-val b ، s 〉

  op1 : ∀ e₁ op e₂ s e₁' s' →
    〈 e₁ ، s 〉 ⇝ 〈 e₁' ، s' 〉
    → --------------------------------------------------------------
    〈 e₁ ′ op ′ e₂ ، s 〉 ⇝ 〈 e₁' ′ op ′ e₂ ، s' 〉

  op2 : ∀ e₁ op e₂ s e₂' s' →
    value e₁ →
    〈 e₂ ، s 〉 ⇝ 〈 e₂' ، s' 〉
    → --------------------------------------------------------------
    〈 e₁ ′ op ′ e₂ ، s 〉 ⇝ 〈 e₁ ′ op ′ e₂' ، s' 〉

  deref : ∀ ℓ s n →
    (p : ℓ ∈ dom s) →
    s lookup[ p ] ℓ ≡ n →
    ------------------------------------------------------------
    〈 ! ℓ ، s 〉 ⇝ 〈 n-val n ، s 〉
  
  assign1 : ∀ ℓ s n →
    ℓ ∈ dom s →
    -----------------------------------------------
    〈 ℓ := n-val n ، s 〉 ⇝ 〈 skip ، s +[ ℓ ↦ n ] 〉

  assign2 : ∀ e s e' s' ℓ →
    〈 e ، s 〉 ⇝ 〈 e' ، s' 〉
    → ------------------------------
    〈 ℓ := e ، s 〉 ⇝ 〈 ℓ := e' ، s' 〉

  seq1 : ∀ e₂ s →
    ------------------------------
    〈 skip ⍮ e₂ ، s 〉 ⇝ 〈 e₂ ، s 〉

  seq2 : ∀ e₁ e₂ s e₁' s' →
    〈 e₁ ، s 〉 ⇝ 〈 e₁' ، s' 〉
    → ------------------------------
    〈 e₁ ⍮ e₂ ، s 〉 ⇝ 〈 e₁' ⍮ e₂ ، s' 〉

  if1 : ∀ e₂ e₃ s →
    -------------------------------------------
    〈 if (b-val true) then e₂ else' e₃ ، s 〉 ⇝ 〈 e₂ ، s 〉

  if2 : ∀ e₂ e₃ s →
    -------------------------------------------
    〈 if (b-val false) then e₂ else' e₃ ، s 〉 ⇝ 〈 e₃ ، s 〉

  if3 : ∀ e₁ e₂ e₃ s e₁' s' →
    〈 e₁ ، s 〉 ⇝ 〈 e₁' ، s' 〉
    → ---------------------------------------------------------------
    〈 if e₁ then e₂ else' e₃ ، s 〉 ⇝ 〈 if e₁' then e₂ else' e₃ ، s' 〉

  while : ∀ e₁ e₂ s
    → ---------------------------------------------------------------
    〈 while e₁ do' e₂ ، s 〉 ⇝
    〈 if e₁ then e₂ ⍮ (while e₁ do' e₂) else' skip ، s 〉

data 𝕋 : Set where
  int bool unit : 𝕋

TypeEnv = 𝕃 ⇀fin 𝕋𝕃

data _⊢_∶_ (Γ : TypeEnv) : (e : L₁)(T : 𝕋) → Set where
  int : ∀ n →
    ------------------------------------------
    Γ ⊢ n-val n ∶ int

  bool : ∀ b →
    ------------------------------------------
    Γ ⊢ b-val b ∶ bool

  op+ : ∀ e₁ e₂ →
    (e₁:int : Γ ⊢ e₁ ∶ int)(e₂:int : Γ ⊢ e₂ ∶ int)
    → ------------------------------------------
    Γ ⊢ e₁ ′ + ′ e₂ ∶ int

  op≥ : ∀ e₁ e₂ →
    (e₁:int : Γ ⊢ e₁ ∶ int)(e₂:int : Γ ⊢ e₂ ∶ int)
    → ------------------------------------------
    Γ ⊢ e₁ ′ ≥ ′ e₂ ∶ bool

  if : ∀ e₁ e₂ e₃ T →
    (e₁:bool : Γ ⊢ e₁ ∶ bool)(e₂:T : Γ ⊢ e₂ ∶ T)(e₃:T : Γ ⊢ e₃ ∶ T)
    → ---------------------------------------------------
    Γ ⊢ if e₁ then e₂ else' e₃ ∶ T

  assign : ∀ ℓ e →
    (p : ℓ ∈ dom Γ)(Γ[l]=intref : Γ lookup[ p ] ℓ ≡ intref)(e:int : Γ ⊢ e ∶ int)
    → -------------------------------------------------------------------------
    Γ ⊢ ℓ := e ∶ unit

  deref : ∀ ℓ →
    (p : ℓ ∈ dom Γ)(Γ[l]=intref : Γ lookup[ p ] ℓ ≡ intref)
    → ------------------------------------------
    Γ ⊢ ! ℓ ∶ int

  skip : --------------------
    Γ ⊢ skip ∶ unit

  seq : ∀ e₁ e₂ T →
    (e₁:unit : Γ ⊢ e₁ ∶ unit)(e₂:T : Γ ⊢ e₂ ∶ T)
    → ------------------------------------------
    Γ ⊢ e₁ ⍮ e₂ ∶ T

  while : ∀ e₁ e₂ →
    (e₁:bool : Γ ⊢ e₁ ∶ bool)(e₂:unit : Γ ⊢ e₂ ∶ unit)
    → ------------------------------------------
    Γ ⊢ while e₁ do' e₂ ∶ unit
