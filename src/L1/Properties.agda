{-# OPTIONS --exact-split --safe  #-}
module L1.Properties where

open import L1.Definition

open import Relation.Nullary
open import Relation.Nullary.Decidable
open import Relation.Binary.PropositionalEquality

-- Lemma 9
values-don't-reduce : ∀{e} →
  value e
  → ----------------------------------
  ∀ s e' s' → ¬ 〈 e ، s 〉 ⇝ 〈 e' ، s' 〉
values-don't-reduce (n-val n) _ _ _ ()

open import Data.Product renaming (_×_ to _∧_)
open import Data.Sum renaming (_⊎_ to _∨_)
open import Data.Integer as ℤ

-- Theorem 1
determinacy : ∀{e s e₁ s₁ e₂ s₂} →
  〈 e ، s 〉 ⇝ 〈 e₁ ، s₁ 〉 →
  〈 e ، s 〉 ⇝ 〈 e₂ ، s₂ 〉 →
  ------------------------------
  〈 e₁ ، s₁ 〉 ≡ 〈 e₂ ، s₂ 〉

open import Data.List.Relation.Binary.Subset.Propositional

open import FinitePartialFunction

-- Theorem 2
progress : ∀{Γ e T s} →
  Γ ⊢ e ∶ T →
  dom Γ ⊆ dom s →
  ------------------------------
  value e ∨ ∃₂ λ e' s' → 〈 e ، s 〉 ⇝ 〈 e' ، s' 〉

-- Theorem 3
type-preservation : ∀{Γ e T s e' s'} →
  Γ ⊢ e ∶ T →
  dom Γ ⊆ dom s →
  〈 e ، s 〉 ⇝ 〈 e' ، s' 〉 →
  ------------------------------
  Γ ⊢ e' ∶ T ∧ dom Γ ⊆ dom s'

open import Relation.Binary.Construct.Closure.ReflexiveTransitive
_⇝*_ = Star _⇝_

-- Theorem 4
safety : ∀{Γ e T s e' s'} →
  Γ ⊢ e ∶ T →
  dom Γ ⊆ dom s →
  〈 e ، s 〉 ⇝* 〈 e' ، s' 〉 →
  ------------------------------
  value e' ∨ ∃₂ λ e″ s″ → 〈 e' ، s' 〉 ⇝ 〈 e″ ، s″ 〉

-- Theorem 5
type-inference : ∀ Γ e → Dec (∃ λ T → Γ ⊢ e ∶ T)

-- Theorem 6
decidability-of-type-checking : ∀ Γ e T → Dec (Γ ⊢ e ∶ T)

-- Theorem 7
uniqueness-of-typing : ∀{Γ e T T'} →
  Γ ⊢ e ∶ T →
  Γ ⊢ e ∶ T' →
  ----------------------------------------
  T ≡ T'

open import Data.Bool

determinacy {while e do' e₁}(while .e .e₁ _)(while .e .e₁ _) = refl
determinacy {(! ℓ₁)} (deref ℓ₁ _ _ _ refl)(deref ℓ₁ _ _ _ refl) = refl
determinacy {ℓ₁ := e} (assign2 e _ e' _ ℓ₁ p')(assign2 e _ e'' _ ℓ₁ p'')
  with refl ← determinacy p' p'' = refl
determinacy {ℓ₁ := n-val n}(assign1 ℓ₁ _ n _)(assign1 ℓ₁ _ n _) = refl
determinacy {.skip ⍮ e₁}(seq1 .e₁ _)(seq1 .e₁ _) = refl
determinacy {e ⍮ e₁}(seq2 .e .e₁ _ e₁' _ p')(seq2 .e .e₁ _ e₁'' _ p'')
  with refl ← determinacy p' p'' = refl
determinacy {.(n-val n₁) ′ .+ ′ .(n-val n₂)}
  (op+ n₁ n₂ _ _ refl)(op+ .n₁ .n₂ _ _ refl) = refl
determinacy {.(n-val n₁) ′ .≥ ′ .(n-val n₂)}
  (op≥ n₁ n₂ _ _ refl)(op≥ .n₁ .n₂ _ _ refl) = refl
determinacy {e ′ op ′ e₁}
  (op1 .e .op .e₁ _ e₁' _ p')(op1 .e .op .e₁ _ e₁'' _ p'')
  with refl ← determinacy p' p'' = refl
determinacy {e ′ op ′ e₁}
  (op2 .e .op .e₁ _ e₂' _ x p')(op2 .e .op .e₁ _ e₂'' _ x₁ p'')
  with refl ← determinacy p' p'' = refl
determinacy {e ′ op ′ e₁}
  (op1 .e .op .e₁ s e₁' s' p')(op2 .e .op .e₁ _ e₂' _ value-e₁ p'')
  with () ← values-don't-reduce value-e₁ s e₁' s' p'
determinacy {e ′ op ′ e₁}
  (op2 .e .op .e₁ _ e₂' _ value-e₁ p')(op1 .e .op .e₁ s e₁'' s'' p'')
  with () ← values-don't-reduce value-e₁ s e₁'' s'' p''
determinacy {if .(b-val true) then e₁ else' e₂}
  (if1 .e₁ .e₂ _) (if1 .e₁ .e₂ _) = refl
determinacy {if .(b-val false) then e₁ else' e₂}
  (if2 .e₁ .e₂ _) (if2 .e₁ .e₂ _) = refl
determinacy {if e then e₁ else' e₂}
  (if3 .e .e₁ .e₂ _ e₁' _ p') (if3 .e .e₁ .e₂ _ e₁'' _ p'')
  with refl ← determinacy p' p'' = refl

open import Function
open DecidableA 𝕃-mod._≟_

progress (int n) Γ⊆s = inj₁ (n-val n)
progress (bool b) Γ⊆s = inj₁ (b-val b)
progress skip Γ⊆s = inj₁ skip
progress {s = s} (op+ e₁ e₂ e₁:int e₂:int) Γ⊆s with progress e₁:int Γ⊆s
... | inj₂ (e₁' , s' , e₁s⇝e₁'s') =
  inj₂ (e₁' ′ + ′ e₂ , s' , op1 e₁ _ e₂ s e₁' s' e₁s⇝e₁'s')
... | inj₁ value-e₁ with progress e₂:int Γ⊆s
... | inj₂ (e₂' , s' , e₂s⇝e₂'s') =
  inj₂ (e₁ ′ + ′ e₂' , s' , op2 e₁ _ e₂ s e₂' s' value-e₁ e₂s⇝e₂'s')
progress {s = s} _ _ | inj₁ (n-val n₁) | inj₁ (n-val n₂) =
  inj₂ (n-val (n₁ ℤ.+ n₂) , s , op+ n₁ n₂ _ s refl)
progress {s = s} (op≥ e₁ e₂ e₁:int e₂:int) Γ⊆s with progress e₁:int Γ⊆s
... | inj₂ (e₁' , s' , e₁s⇝e₁'s') =
  inj₂ (e₁' ′ ≥ ′ e₂ , s' , op1 e₁ _ e₂ s e₁' s' e₁s⇝e₁'s')
... | inj₁ value-e₁ with progress e₂:int Γ⊆s
... | inj₂ (e₂' , s' , e₂s⇝e₂'s') =
  inj₂ (e₁ ′ ≥ ′ e₂' , s' , op2 e₁ _ e₂ s e₂' s' value-e₁ e₂s⇝e₂'s')
progress {s = s} _ _ | inj₁ (n-val n₁) | inj₁ (n-val n₂) =
  inj₂ (b-val ⌊ n₂ ℤ.≤? n₁ ⌋ , s , op≥ n₁ n₂ _ s refl)
progress {s = s} (if e₁ e₂ e₃ _ e₁:bool e₂:T e₃:T) Γ⊆s with progress e₁:bool Γ⊆s
... | inj₁ (b-val true) = inj₂ (e₂ , s , if1 e₂ e₃ s)
... | inj₁ (b-val false) = inj₂ (e₃ , s , if2 e₂ e₃ s)
... | inj₂ (e₁' , s' , e₁s⇝e₁'s') =
  inj₂ (if e₁' then e₂ else' e₃ , s' , if3 e₁ e₂ e₃ s e₁' s' e₁s⇝e₁'s')
progress {s = s} (assign ℓ₀ e p Γ[l]=intref e:int) Γ⊆s with progress e:int Γ⊆s
... | inj₁ (n-val n) = inj₂ (skip , s +[ ℓ₀ ↦ n ] , assign1 ℓ₀ s n (Γ⊆s p))
... | inj₂ (e' , s' , es⇝e's') =
  inj₂ (ℓ₀ := e' , s' , assign2 e s e' s' ℓ₀ es⇝e's')
progress {s = s} (deref ℓ₀ p Γ[l]=intref) Γ⊆s =
  inj₂ (n-val (s lookup[ Γ⊆s p ] ℓ₀) , s , deref ℓ₀ s _ (Γ⊆s p) refl)
progress {s = s} (seq e₁ e₂ _ e₁:unit e₂:T) Γ⊆s with progress e₁:unit Γ⊆s
... | inj₁ skip = inj₂ (e₂ , s , seq1 e₂ s)
... | inj₂ (e₁' , s' , e₁s⇝e₁'s') =
  inj₂ (e₁' ⍮ e₂ , s' , seq2 e₁ e₂ s e₁' s' e₁s⇝e₁'s')
progress {s = s} (while e₁ e₂ e₁:bool e₂:unit) Γ⊆s =
  inj₂ (if e₁ then e₂ ⍮ (while e₁ do' e₂) else' skip , s , while e₁ e₂ s)

type-preservation (op+ _ _ e:T e:T₁) Γ⊆s (op+ n₁ n₂ n _ x) = int n , Γ⊆s
type-preservation (op+ e₁ e₂ e₁:int e₂:int) Γ⊆s (op1 _ _ _ _ e₁' _ e₁s⇝e₁'s')
  with e₁':int , Γ⊆s' ← type-preservation e₁:int Γ⊆s e₁s⇝e₁'s' =
  op+ e₁' e₂ e₁':int e₂:int , Γ⊆s'
type-preservation (op+ e₁ e₂ e₁:int e₂:int) Γ⊆s (op2 _ _ _ _ e₂' _ x e₂s⇝e₂'s')
  with e₂':int , Γ⊆s' ← type-preservation e₂:int Γ⊆s e₂s⇝e₂'s' =
  op+ e₁ e₂' e₁:int e₂':int , Γ⊆s'
type-preservation (op≥ _ _ e:T e:T₁) Γ⊆s (op≥ n₁ n₂ b _ x) = bool b , Γ⊆s
type-preservation (op≥ e₁ e₂ e₁:int e₂:int) Γ⊆s (op1 _ _ _ _ e₁' _ e₁s⇝e₁'s')
  with e₁':int , Γ⊆s' ← type-preservation e₁:int Γ⊆s e₁s⇝e₁'s' =
  op≥ e₁' e₂ e₁':int e₂:int , Γ⊆s'
type-preservation (op≥ e₁ e₂ e₁:int e₂:int) Γ⊆s (op2 _ _ _ _ e₂' _ x e₂s⇝e₂'s')
  with e₂':int , Γ⊆s' ← type-preservation e₂:int Γ⊆s e₂s⇝e₂'s' =
  op≥ e₁ e₂' e₁:int e₂':int , Γ⊆s'
type-preservation (if .(b-val true) e₂ _ T _ e₂:T _) Γ⊆s (if1 .e₂ _ _) =
  e₂:T , Γ⊆s
type-preservation (if .(b-val false) _ e₃ T _ _ e₃:T) Γ⊆s (if2 _ .e₃ _) =
  e₃:T , Γ⊆s
type-preservation (if e₁ e₂ e₃ T e₁:bool e₂:T e₃:T) Γ⊆s
                  (if3 .e₁ .e₂ .e₃ _ e₁' _ e₁s⇝e₁'s')
  with e₁':bool , Γ⊆s' ← type-preservation e₁:bool Γ⊆s e₁s⇝e₁'s' =
  if e₁' e₂ e₃ T e₁':bool e₂:T e₃:T , Γ⊆s'
type-preservation (assign ℓ₀ .(n-val n) p _ e:T) Γ⊆s (assign1 .ℓ₀ s n x) =
  skip , λ {x} x∈domΓ → Equivalence.g (∈dom+⇔dom s ℓ₀ n (Γ⊆s p) x) (Γ⊆s x∈domΓ)
type-preservation (assign ℓ₀ e p _ e:int) Γ⊆s (assign2 .e _ e' _ .ℓ₀ es⇝e's')
  with e':int , Γ⊆s' ← type-preservation e:int Γ⊆s es⇝e's' =
  assign ℓ₀ e' p refl e':int , Γ⊆s'
type-preservation (deref ℓ₀ p Γ[l]=intref) Γ⊆s (deref .ℓ₀ _ n p₁ x) = int n , Γ⊆s
type-preservation (seq .skip e₂ _ _ e₂:T) Γ⊆s (seq1 .e₂ _) = e₂:T , Γ⊆s
type-preservation (seq e₁ e₂ T e₁:unit e₂:T) Γ⊆s (seq2 _ _ _ e₁' _ e₁s⇝e₁'s')
  with e₁':unit , Γ⊆s' ← type-preservation e₁:unit Γ⊆s e₁s⇝e₁'s' =
  seq e₁' e₂ T e₁':unit e₂:T , Γ⊆s'
type-preservation t@(while e₁ e₂ e₁:bool e₂:unit) Γ⊆s (while .e₁ .e₂ _) =
  if e₁ (e₂ ⍮ (while e₁ do' e₂)) skip unit
    e₁:bool
    (seq e₂ (while e₁ do' e₂) unit e₂:unit t)
    skip ,
  Γ⊆s

safety e:T Γ⊆s ε = progress e:T Γ⊆s
safety e:T Γ⊆s (es⇝e″s″ ◅ e″s″⇝*e's')
  with e″:T , Γ⊆s″ ← type-preservation e:T Γ⊆s es⇝e″s″ =
  safety e″:T Γ⊆s″ e″s″⇝*e's'

open import Data.List.Membership.DecPropositional 𝕃-mod._≟_

type-inference Γ (n-val n) = yes (int , int n)
type-inference Γ (b-val b) = yes (bool , bool b)
type-inference Γ skip = yes (unit , skip)
type-inference Γ (e₁ ⍮ e₂)
  with decidability-of-type-checking Γ e₁ unit | type-inference Γ e₂
... | no proof₁ | _ =
  no λ { (_ , seq .e₁ .e₂ _ e₁:unit _) → proof₁ e₁:unit}
... | yes proof₁ | no proof₂ =
  no λ { (T , seq .e₁ .e₂ .T _ e₂:T) → proof₂ (T , e₂:T)}
... | yes e₁:unit | yes (T , e₂:T) =
  yes (T , seq e₁ e₂ T e₁:unit e₂:T)
type-inference Γ (while e₁ do' e₂)
  with decidability-of-type-checking Γ e₁ bool |
       decidability-of-type-checking Γ e₂ unit
... | yes e₁:bool | yes e₂:unit = yes (unit , while e₁ e₂ e₁:bool e₂:unit)
... | yes e₁:bool | no proof₂ =
  no λ { (.unit , while .e₁ .e₂ e₁:bool e₂:unit) → proof₂ e₂:unit}
... | no proof₁ | _ =
  no λ { (.unit , while .e₁ .e₂ e₁:bool e₂:unit) → proof₁ e₁:bool}
type-inference Γ (e₁ ′ + ′ e₂)
  with decidability-of-type-checking Γ e₁ int |
       decidability-of-type-checking Γ e₂ int
... | yes e₁:int | yes e₂:int = yes (int , op+ e₁ e₂ e₁:int e₂:int)
... | yes e₁:int | no proof₂ =
  no λ { (.int , op+ .e₁ .e₂ e₁:int e₂:int) → proof₂ e₂:int}
... | no proof₁ | _ =
  no λ { (.int , op+ .e₁ .e₂ e₁:int e₂:int) → proof₁ e₁:int}
type-inference Γ (e₁ ′ ≥ ′ e₂)
  with decidability-of-type-checking Γ e₁ int |
       decidability-of-type-checking Γ e₂ int
... | yes e₁:int | yes e₂:int = yes (bool , op≥ e₁ e₂ e₁:int e₂:int)
... | yes e₁:int | no proof₂ =
  no λ { (.bool , op≥ .e₁ .e₂ e₁:int e₂:int) → proof₂ e₂:int}
... | no proof₁ | _ =
  no λ { (.bool , op≥ .e₁ .e₂ e₁:int e₂:int) → proof₁ e₁:int}
type-inference Γ (! ℓ₀) with ℓ₀ ∈? dom Γ
... | yes ℓ₀∈domΓ = yes (int , deref ℓ₀ ℓ₀∈domΓ refl)
... | no ℓ₀∉domΓ = no λ { (.int , deref .ℓ₀ p Γ[l]=intref) → ℓ₀∉domΓ p}
type-inference Γ (ℓ₀ := e)
  with decidability-of-type-checking Γ e int | ℓ₀ ∈? dom Γ
... | yes e:int | yes ℓ₀∈domΓ = yes (unit , assign ℓ₀ e ℓ₀∈domΓ refl e:int)
... | yes e:int | no ℓ₀∉domΓ =
  no λ { (.unit , assign .ℓ₀ .e p Γ[l]=intref e:int) → ℓ₀∉domΓ p}
... | no proof₁ | _ =
  no λ { (.unit , assign .ℓ₀ .e p Γ[l]=intref e:int) → proof₁ e:int}
type-inference Γ (if e₁ then e₂ else' e₃) with type-inference Γ e₂
... | no proof₂ =
  no λ { (T , if .e₁ .e₂ .e₃ .T e₁:bool e₂:T e₃:T) → proof₂ (T , e₂:T)}
... | yes (T , e₂:T) with decidability-of-type-checking Γ e₁ bool |
                          decidability-of-type-checking Γ e₃ T
... | yes e₁:bool | yes e₃:T = yes (T , if e₁ e₂ e₃ T e₁:bool e₂:T e₃:T)
... | yes e₁:bool | no proof₃ =
  no λ { (T' , if .e₁ .e₂ .e₃ .T' e₁:bool e₂:T' e₃:T') →
  case uniqueness-of-typing e₂:T e₂:T' of λ { refl → proof₃ e₃:T'} }
... | no proof₁ | _ =
  no λ { (T' , if .e₁ .e₂ .e₃ .T' e₁:bool e₂:T' e₃:T') → proof₁ e₁:bool}


decidability-of-type-checking Γ (n-val n) int = yes (int n)
decidability-of-type-checking Γ (n-val n) bool = no λ ()
decidability-of-type-checking Γ (n-val n) unit = no λ ()
decidability-of-type-checking Γ (b-val b) int = no λ ()
decidability-of-type-checking Γ (b-val b) bool = yes (bool b)
decidability-of-type-checking Γ (b-val b) unit = no λ ()
decidability-of-type-checking Γ skip int = no λ ()
decidability-of-type-checking Γ skip bool = no λ ()
decidability-of-type-checking Γ skip unit = yes skip
decidability-of-type-checking Γ (e₁ ′ op ′ e₂) unit = no λ ()
decidability-of-type-checking Γ (e₁ ′ ≥ ′ e₂) int = no λ ()
decidability-of-type-checking Γ (e₁ ′ + ′ e₂) int
  with decidability-of-type-checking Γ e₁ int |
       decidability-of-type-checking Γ e₂ int
... | yes e₁:int | yes e₂:int = yes (op+ e₁ e₂ e₁:int e₂:int)
... | yes e₁:int | no proof₂ =
  no λ { (op+ .e₁ .e₂ e₁:int e₂:int) → proof₂ e₂:int}
... | no proof₁ | _ =
  no λ { (op+ .e₁ .e₂ e₁:int e₂:int) → proof₁ e₁:int}
decidability-of-type-checking Γ (e₁ ′ + ′ e₂) bool = no λ ()
decidability-of-type-checking Γ (e₁ ′ ≥ ′ e₂) bool
  with decidability-of-type-checking Γ e₁ int |
       decidability-of-type-checking Γ e₂ int
... | yes e₁:int | yes e₂:int = yes (op≥ e₁ e₂ e₁:int e₂:int)
... | yes e₁:int | no proof₂ =
  no λ { (op≥ .e₁ .e₂ e₁:int e₂:int) → proof₂ e₂:int}
... | no proof₁ | _ =
  no λ { (op≥ .e₁ .e₂ e₁:int e₂:int) → proof₁ e₁:int}
decidability-of-type-checking Γ (if e₁ then e₂ else' e₃) T
  with decidability-of-type-checking Γ e₁ bool |
       decidability-of-type-checking Γ e₂ T |
       decidability-of-type-checking Γ e₃ T
... | yes e₁:bool | yes e₂:T | yes e₃:T = yes (if e₁ e₂ e₃ T e₁:bool e₂:T e₃:T)
... | yes e₁:bool | yes e₂:T | no proof₃ =
  no λ { (if .e₁ .e₂ .e₃ .T e₁:bool e₂:T e₃:T) → proof₃ e₃:T}
... | yes e₁:bool | no proof₂ | _ =
  no λ { (if .e₁ .e₂ .e₃ .T e₁:bool e₂:T e₃:T) → proof₂ e₂:T}
... | no proof₁ | _ | _ =
  no λ { (if .e₁ .e₂ .e₃ .T e₁:bool e₂:T e₃:T) → proof₁ e₁:bool}
decidability-of-type-checking Γ (ℓ₀ := e) unit
  with decidability-of-type-checking Γ e int |
       ℓ₀ ∈? dom Γ
... | yes e:int | yes ℓ₀∈domΓ = yes (assign ℓ₀ e ℓ₀∈domΓ refl e:int)
... | yes e:int | no proof₂ =
  no λ { (assign .ℓ₀ .e p Γ[l]=intref e:int) → proof₂ p}
... | no proof₁ | _ =
  no λ { (assign .ℓ₀ .e p Γ[l]=intref e:int) → proof₁ e:int}
decidability-of-type-checking Γ (ℓ₀ := e) int = no λ ()
decidability-of-type-checking Γ (ℓ₀ := e) bool = no λ ()
decidability-of-type-checking Γ (! ℓ₀) int with ℓ₀ ∈? dom Γ
... | yes ℓ₀∈domΓ = yes (deref ℓ₀ ℓ₀∈domΓ refl)
... | no ℓ₀∉domΓ = no λ { (deref ℓ₀ ℓ₀∈domΓ refl) → ℓ₀∉domΓ ℓ₀∈domΓ}
decidability-of-type-checking Γ (! ℓ₀) bool = no λ ()
decidability-of-type-checking Γ (! ℓ₀) unit = no λ ()
decidability-of-type-checking Γ (e₁ ⍮ e₂) T
  with decidability-of-type-checking Γ e₁ unit |
       decidability-of-type-checking Γ e₂ T
... | yes e₁:unit | yes e₂:T = yes (seq e₁ e₂ T e₁:unit e₂:T)
... | yes e₁:unit | no proof₂ =
  no λ { (seq .e₁ .e₂ .T e₁:unit e₂:T) → proof₂ e₂:T}
... | no proof₁ | _ =
  no λ { (seq .e₁ .e₂ .T e₁:unit e₂:T) → proof₁ e₁:unit}
decidability-of-type-checking Γ (while e₁ do' e₂) int = no λ ()
decidability-of-type-checking Γ (while e₁ do' e₂) bool = no λ ()
decidability-of-type-checking Γ (while e₁ do' e₂) unit
  with decidability-of-type-checking Γ e₁ bool |
       decidability-of-type-checking Γ e₂ unit
... | yes e₁:bool | yes e₂:unit = yes (while e₁ e₂ e₁:bool e₂:unit)
... | yes proof₁ | no proof₂ =
  no λ { (while .e₁ .e₂ e₁:bool e₂:unit) → proof₂ e₂:unit}
... | no proof₁ | _ =
  no λ { (while .e₁ .e₂ e₁:bool e₂:unit) → proof₁ e₁:bool}

uniqueness-of-typing (int n) (int .n) = refl
uniqueness-of-typing (bool b) (bool .b) = refl
uniqueness-of-typing (op+ e₁ e₂ e:T e:T₁)(op+ .e₁ .e₂ e:T' e:T'') = refl
uniqueness-of-typing (op≥ e₁ e₂ e:T e:T₁)(op≥ .e₁ .e₂ e:T' e:T'') = refl
uniqueness-of-typing (if _ _ _ _ _ e₂:T _)(if _ _ _ _ _ e₂:T' _) =
  uniqueness-of-typing e₂:T e₂:T'
uniqueness-of-typing (assign _ _ _ _ _)(assign _ _ _ _ _) = refl
uniqueness-of-typing (deref _ _ _)(deref _ _ _) = refl
uniqueness-of-typing skip skip = refl
uniqueness-of-typing (seq _ _ _ _ e₂:T)(seq _ _ _ _ e₂:T') =
  uniqueness-of-typing e₂:T e₂:T'
uniqueness-of-typing (while e₁ e₂ e:T e:T₁) (while .e₁ .e₂ e:T' e:T'') = refl
