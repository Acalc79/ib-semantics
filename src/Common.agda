{-# OPTIONS --exact-split --safe  #-}
module Common where

open import Data.Nat as ℕ hiding (_≟_)

record 𝕃 : Set where
  constructor ℓ
  field
    ℓ : ℕ

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

module 𝕃-mod where
  _≟_ : (ℓ₁ ℓ₂ : 𝕃) → Dec (ℓ₁ ≡ ℓ₂)
  ℓ a ≟ ℓ b with a ℕ.≟ b
  ... | yes refl = yes refl
  ... | no ¬a≡b = no λ { refl → ¬a≡b refl}

open import Relation.Nullary.Decidable

data Op : Set where
  + ≥ : Op

open import FinitePartialFunction
import Data.Integer as Int
open Int
open Int using (ℤ) public

Store : Set
Store = 𝕃 ⇀fin ℤ

record Config (L : Set) : Set where
  constructor 〈_،_〉
  field
    expr : L
    store : Store

record 𝕋𝕃 : Set where
  constructor intref

𝕋𝕃-irrelevant : Irrelevant 𝕋𝕃
𝕋𝕃-irrelevant _ _ = refl
