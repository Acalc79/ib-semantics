{-# OPTIONS --exact-split --safe  #-}
module FiniteSubset where

open import Level

private
  variable a l : Level

open import Data.List as List hiding (filter)
open import Data.List.Relation.Unary.Unique.Propositional
open import Data.Product

private
  module Private where
    record FiniteSubset (X : Set a) : Set a where
      constructor [_،_]
      field
        list : List X
        .uniq : Unique list

    open FiniteSubset using (list) public

FiniteSubset = Private.FiniteSubset
list = Private.list
open Private using ([_،_])

private
  variable X : Set a

∅ : FiniteSubset X
∅ = [ [] ، [] ]

open import Data.List.Relation.Unary.All

｛_｝ : (x : X) → FiniteSubset X
｛ x ｝ = [ [ x ] ، [] ∷ [] ]

open import Relation.Unary using (Decidable)

open import Data.List.Relation.Unary.Unique.Propositional.Properties
  
filter : {P : X → Set l}
  (P? : Decidable P)(A : FiniteSubset X)
  → ----------------------------------------
  FiniteSubset X
filter P? [ list ، uniq ] = [ List.filter P? list ، filter⁺ P? uniq ]

open import Relation.Nullary
open import Relation.Nullary.Negation
open import Relation.Binary.PropositionalEquality
open import Function

module _ {U : Set l} where
  infix 4 _∈_ _∉_ _⊆_
  _∈_ _∉_ : (a : U)(A : FiniteSubset U) → Set l
  a ∈ [ A ، _ ] = a L.∈ A
    where import Data.List.Membership.Propositional as L
  a ∉ A = ¬ a ∈ A

  _⊆_ : (A B : FiniteSubset U) → Set l
  A ⊆ B = ∀{x} → x ∈ A → x ∈ B

  ∈-∅ : (a : U) → a ∉ ∅
  ∈-∅ _ ()
  
  ∈-｛_｝ : (a x : U) → x ∈ ｛ a ｝ ⇔ a ≡ x
  ∈-｛ a ｝ x = mk⇔ (λ { (here refl) → refl}) λ {refl → here refl}
    where open import Data.List.Relation.Unary.Any
  
  open import Data.List.Membership.Propositional.Properties as P

  ∈-filter : {P : U → Set a}(P? : Decidable P)(A : FiniteSubset U)
    → ---------------------------------------------------------------
    ∀{a} → a ∈ filter P? A ⇔ (a ∈ A × P a)
  ∈-filter P? _ = mk⇔ (P.∈-filter⁻ P?)
                      (λ {(a∈A , Pa) → P.∈-filter⁺ P? a∈A Pa})
  
  module DecEquality (_≟_ : (a b : U) → Dec (a ≡ b)) where
    import Data.List.Membership.DecPropositional _≟_ as L
    infixr 7 _∩_
    infixr 6 _∪_
    infixr 5 _-_
    _∩_ _∪_ _-_ : (A B : FiniteSubset U) → FiniteSubset U
  
    _∈?_ : (a : U)(A : FiniteSubset U) → Dec (a ∈ A)
    a ∈? [ A ، _ ] = a L.∈? A
  
    A ∩ [ B ، _ ] = filter (L._∈? B) A
    A - [ B ، _ ] = filter (¬? ∘ (L._∈? B)) A
  
    ∈-∩ : ∀ A B {a} → a ∈ A ∩ B ⇔ (a ∈ A × a ∈ B)
    ∈-∩ A [ B ، _ ] = ∈-filter (L._∈? B) A
  
    ∈-- : ∀ A B {a} → a ∈ A - B ⇔ (a ∈ A × a ∉ B)
    ∈-- A [ B ، _ ] = ∈-filter (¬? ∘ (L._∈? B)) A
  
    open Equivalence renaming (f to to; g to from)
    
    A@([ ls ، !A ]) ∪ B@([ ls' ، _ ]) = let [ ls″ ، !B' ] = B - A in
      [ ls ++ ls″ ،
        ++⁺ !A !B' (λ { (a∈A , a∈B') →
          case to (∈-- B A) a∈B' of λ { (_ , a∉A) → a∉A a∈A}}) ]
  
    open import Data.Sum
  
    ∈-∪ : ∀ A B {a} → a ∈ A ∪ B ⇔ (a ∈ A ⊎ a ∈ B)
    ∈-∪ A@([ ls ، _ ])B@([ ls' ، _ ]) {a} = mk⇔
      (λ a∈A∪B → case ∈-++⁻ ls a∈A∪B of λ
        { (inj₁ a∈A) → inj₁ a∈A
        ; (inj₂ a∈B-A) → inj₂ $ proj₁ $ to (∈-- B A) a∈B-A})
      λ { (inj₁ a∈A) → ∈-++⁺ˡ a∈A
        ; (inj₂ a∈B) → case a ∈? A of λ
          { (yes a∈A) → ∈-++⁺ˡ a∈A
          ; (no a∉A) → ∈-++⁺ʳ ls (from (∈-- B A) (a∈B , a∉A)) } }
      where open Equivalence renaming (f to to; g to from)
  
    infix 10 ⋂ ⋃
  
    ⋃ : (S : FiniteSubset (FiniteSubset U)) → FiniteSubset U
    ⋃ [ S ، _ ] = foldr _∪_ ∅ S
  
    ⋂ : (S : FiniteSubset (FiniteSubset U)) → FiniteSubset U
    -- we don't have a good option here, so choose convention ⋂ ∅ = ∅ 
    ⋂ [ [] ، _ ] = ∅
    ⋂ [ A ∷ S ، _ ] = foldr _∩_ A S
