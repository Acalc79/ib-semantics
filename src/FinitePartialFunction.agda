{-# OPTIONS --exact-split --safe  #-}
module FinitePartialFunction where

open import Level
open import Data.List as L hiding (lookup; [_]; tail)
open import Data.Maybe hiding (map)
open import Data.Product hiding (map)
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality
open import Function

private
  variable a b : Level
           A : Set a
           B : (a : A) → Set b

open import Data.Bool
open import FiniteSubset as Fin using (FiniteSubset; _∈_; _∉_; _⊆_; ∅; ｛_｝)
open import Relation.Binary using (DecidableEquality)

private
  module Private where
    record _⇀_ (A : Set a)(B : (a : A) → Set b) : Set (a ⊔ b) where
      constructor [_،_،_]
      open Fin.DecEquality _≟_
      field
        dom : FiniteSubset A
        func : (a : A) → Maybe (B a)
        .prop : ∀ a → T (is-just $ func a) ⇔ a ∈ dom

open Private

_⇀fin_ : (A : Set a)(B : Set b) → Set _
A ⇀fin B = A ⇀ λ _ → B
_⇀dep_ = _⇀_
dom = _⇀_.dom
func = _⇀_.func

⦃⦄ : A ⇀ B
⦃⦄ = [ ∅ ، (λ _ → nothing) ، (λ _ → mk⇔ (λ ()) λ ()) ]

ran : {B : Set a} → A ⇀fin B → List B
ran f = concatMap L.fromMaybe $ L.map (func f) $ Fin.list $ dom f

open Equivalence renaming (f to to; g to from)

open import Data.Empty

lookup : (f : A ⇀ B)(a : A).(p : a ∈ dom f) → B a
lookup [ dom ، f ، prop ] a p with f a | inspect f a
... | just b | _ = b
... | nothing | [ fa≡nothing ] =
  ⊥-elim $ escape (subst (T ∘ is-just) fa≡nothing $ from (prop a) p)
  where escape : .⊥ → ⊥
        escape ()

syntax lookup s ℓ p = s lookup[ p ] ℓ

lookup-result :
  (f : A ⇀ B)
  (a : A){b : B a}
  .(p : a ∈ dom f)
  (q : func f a ≡ just b)
  → ------------------------
  f lookup[ p ] a ≡ b
lookup-result f a p q with func f a | inspect (func f) a
lookup-result f a p refl | just _ | _ = refl

open import Data.Unit

module DecEquality {A : Set a}(_≟_ : DecidableEquality A) where

  open import Data.List.Relation.Unary.Any hiding (lookup)

  singleton : (a : A)(b : B a) → A ⇀ B
  singleton {B = B} a b =
    [ ｛ a ｝ ، f ، (λ x → mk⇔ (prop₁ x) (prop₂ x)) ]
    where f : (a : A) → Maybe (B a)
          f x with a ≟ x
          ... | yes refl = just b
          ... | no _     = nothing
          prop₁ : ∀ x → T (is-just $ f x) → x ∈ ｛ a ｝
          prop₁ x p with a ≟ x
          ... | yes refl = here refl
          prop₁ _ () | no _
          prop₂ : ∀ x → x ∈ ｛ a ｝ → T (is-just $ f x)
          prop₂ _ (here refl) with a ≟ a
          ... | yes refl = tt
          ... | no x≢x = x≢x refl

  syntax singleton a b = ｛ b / a ｝

  open Fin.DecEquality _≟_
  open Fin using (∈-｛_｝; ∈-∅)
  open import Relation.Nullary.Decidable
  open import Data.Sum
  
  _+[_↦_] : (f : A ⇀ B)(a : A)(b : B a) → A ⇀ B
  _+[_↦_] {B = B} [ dom ، f ، prop ] a b =
    [ dom' ، f' ، (λ x → mk⇔ (prop₁ x $ to $ prop x) (prop₂ x $ from $ prop x)) ]
    where dom' = ｛ a ｝ ∪ dom
          f' : (x : A) → Maybe (B x)
          f' x with a ≟ x
          ... | yes refl = just b
          ... | no _ = f x
          prop₁ : ∀ x (p : T (is-just $ f x) → x ∈ dom)
            → --------------------------------------------
            T (is-just $ f' x) → x ∈ dom'          
          prop₁ x p q with a ≟ x
          ... | yes refl = from (∈-∪ ｛ a ｝ dom) $
                           inj₁ $
                           from (∈-｛ x ｝ x) refl
          ... | no _ = from (∈-∪ ｛ a ｝ dom) $ inj₂ $ p q
          prop₂ : ∀ x (p : x ∈ dom → T (is-just $ f x))
            → --------------------------------------------------
            x ∈ dom' → T (is-just $ f' x)
          prop₂ x p q with to (∈-∪ ｛ a ｝ dom) q | a ≟ x
          ... | _ | yes refl = tt
          ... | inj₁ x∈｛a｝ | no a≢x with () ← a≢x $ to (∈-｛ a ｝ x) x∈｛a｝
          ... | inj₂ x∈dom | no _ = p x∈dom

  from-list : List (Σ A B) → A ⇀ B
  from-list [] = ⦃⦄
  from-list ((a , b) ∷ ls) = from-list ls +[ a ↦ b ]

  dom⊆dom+ :
    (f : A ⇀ B)
    (a : A)(b : B a)
    → ------------------------------------
    dom f ⊆ dom (f +[ a ↦ b ])
  dom⊆dom+ f a b x = from (∈-∪ ｛ a ｝ (dom f)) $ inj₂ x

  ∈dom+⇒dom :
    (f : A ⇀ B)
    (a : A)(b : B a)
    (a∈dom : a ∈ dom f)
    → ------------------------------------
    dom (f +[ a ↦ b ]) ⊆ dom f
  ∈dom+⇒dom f a b a∈dom {x} x∈dom-f-∪-｛a｝
    with to (∈-∪ ｛ a ｝ (dom f)) x∈dom-f-∪-｛a｝
  ... | inj₁ x∈｛a｝ with refl ← to (∈-｛ a ｝ x) x∈｛a｝ = a∈dom
  ... | inj₂ x∈dom-f = x∈dom-f

  private
    record Σ' {l : Level}(P : Set l)(Q : .P → Set b) : Set (l ⊔ b) where
      constructor _,,_
      field
        proj₁' : P
        proj₂' : Q proj₁'

    Σ-dec : {P : Set a}{Q : .P → Set b}
      (?P : Dec P)
      (?Q : .(p : P) → Dec (Q p))
      → ----------------------------------------
      Dec (Σ' P Q)
  
  Σ-dec (no ¬p) ?Q = no λ { (p ,, _) → ⊥-elim $ ¬p p}
  Σ-dec (yes p) ?Q with ?Q p
  ... | (no ¬q) = no λ { (_ ,, q) → escape (¬q q) }
    where escape : .⊥ → ⊥
          escape ()
  ... | (yes q) = yes (p ,, q)
    
  module _ {ℓ₀ ℓ₁ : Level}{B : Set ℓ₀}{C : Set ℓ₁} where
    _∘[_]_ :
      (f : A ⇀fin B)
      (_≟_ : DecidableEquality B)
      (g : B ⇀fin C)
      → --------------------------------
      A ⇀fin C
    F@([ domf ، f ، propf ]) ∘[ _≟_ ] G@([ domg ، g ، propg ]) =
      [ dom' ، f∘g
      ، (λ x → mk⇔ (prop₁ x (to $ propf x)(to ∘ propg))
                   (prop₂ x (from $ propf x)(from ∘ propg))) ]
      where module Fin' = Fin.DecEquality _≟_
            open import Data.Product.Properties
            P? : (a : A) →
              Dec (Σ' (a ∈ domf) λ a∈domf → F lookup[ a∈domf ] a ∈ domg)
            P? a = Σ-dec
              (a ∈? domf)
              (λ a∈domf → (F lookup[ a∈domf ] a) Fin'.∈? domg)
            dom' = Fin.filter P? domf
            f∘g : (a : A) → Maybe C
            f∘g a = f a >>= g
            prop₁ : ∀ x
              (pf : T (is-just $ f x) → x ∈ domf)
              (pg : ∀ x → T (is-just $ g x) → x ∈ domg)
              → --------------------------------------------
              T (is-just $ f∘g x) → x ∈ dom'          
            prop₁ x pf pg q with f x | inspect f x
            ... | just b | [ eq ] with pg b | g b | inspect g b
            ... | pg' | just c | [ eq' ] =
              from (Fin.∈-filter P? domf) $
                pf tt , (pf tt ,,
                subst (_∈ domg) (sym $ lookup-result F x (pf tt) eq)(
                pg' $ subst (T ∘ is-just) (sym eq') tt))
            prop₂ : ∀ x
              (pf : x ∈ domf → T (is-just $ f x))
              (pg : ∀ x → x ∈ domg → T (is-just $ g x))
              → --------------------------------------------------
              x ∈ dom' → T (is-just $ f∘g x)
            prop₂ x pf pg q
              with to (Fin.∈-filter P? domf) q
            ... | x∈domf , (_ ,, fb∈domg) with f x | inspect f x | pf x∈domf
            ... | just b | [ eq ] | _ = pg b fb∈domg
  
-- map-filter : ∀{a b c}{A : Set a}{B : Set b}{P : Pred B c}
--   (f : A → B)(P? : Decidable P) xs
--   → --------------------------------------------------------
--   filter P? (L.map f xs) ≡ L.map f (filter (P? ∘ f) xs)
-- map-filter f P? [] = refl
-- map-filter f P? (x ∷ xs) with does $ P? $ f x
-- ... | true = cong (f x ∷_) $ map-filter f P? xs
-- ... | false = map-filter f P? xs

-- module WithDecidable {A : Set a}(_≟_ : (a b : A) → Dec (a ≡ b)) where

--   open import Data.List.Relation.Binary.Subset.Propositional
--   open import Data.Sum
  
--   open import Data.Bool hiding (_≟_)
  
--   lookup-inj : (f : A ⇀ B)
--     {a a' : A}.(p : a ∈ dom f).(p' : a' ∈ dom f)
--     (a≡a' : a ≡ a')
--     → --------------------------------------------
--     f lookup[ p ] a ≡ subst B (sym a≡a') (f lookup[ p' ] a')
--   lookup-inj f p p' refl = refl
